import pygame
import math
import random
from pygame.sprite import Sprite

class Exhaust(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen, ship,bgoffset_x,bgoffset_y):
        """Create a bullet object at the ship's current position."""
        super(Exhaust, self).__init__()
        self.screen = screen

        # Create a bullet rect at (0,0) and then set the correct position.
        self.rect = pygame.Rect(0,0,ai_settings.bullet_width,ai_settings.bullet_height)
        self.rect.centerx = ship.centerx+int(-10*math.sin(ship.dir_facing))-bgoffset_x+random.randrange(0,4)
        self.rect.centery = ship.centery+int(-10*math.cos(ship.dir_facing))-bgoffset_y+random.randrange(0,4)

        #Store the bullet's position as a decimal value
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)
        self.lifetime = 0
        self.expired = False
        self.zigzagtime_0 = 7 + random.randrange(0,11)
        self.zigzagtime_1 = 14 + random.randrange(0,11)
        self.zigzagtime_2 = 21 + random.randrange(0,11)
        self.zigzagtime_3 = 28 + random.randrange(0,11)
        self.dietime = 35 + random.randrange(0,11)
        
        self.color_index= random.randrange(1,8)
        self.color = (255,150,0)
        self.color=self.getexhaustcolorfromindex(self.color_index)
        self.speed_factor = ai_settings.bullet_speed_factor
    
        self.dir_moving = 0
        self.speed = 0
        self.mass_base=100
        self.can_boost=True
        self.screen_rect = screen.get_rect()
        self.can_hit=True
    
        self.x_vel=ship.x_vel-math.sin(ship.dir_facing)*3+random.gauss(0,.2)
        self.y_vel=ship.y_vel-math.cos(ship.dir_facing)*3+random.gauss(0,.2)


    def update(self):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        self.x += self.x_vel
        self.y += self.y_vel
        #Update the rect position.
        self.rect.x = int(self.x)
        self.rect.y = int(self.y)
        
        if (self.lifetime==self.zigzagtime_0 or self.lifetime==self.zigzagtime_1 or self.lifetime==self.zigzagtime_2 or self.lifetime==self.zigzagtime_3):
            self.zig()
        elif self.lifetime>=self.dietime:
            self.expired=True
        self.lifetime = self.lifetime+1
    
    def zig(self):
        self.x_vel+=random.gauss(0, 1)
        self.y_vel+=random.gauss(0, 1)
        self.can_boost=False
    
    def draw_exhaust(self,gameRect,bgoffset_x,bgoffset_y):
        """Draw the bullet to the screen."""
        rect_move=self.rect.move(bgoffset_x,bgoffset_y)
        
        pygame.draw.rect(self.screen, self.color,rect_move)
        #pygame.draw.rect(self.screen, self.color,self.rect)
        #print(self.rect)

    def getexhaustcolorfromindex(self,color_index):
        if color_index==1:
            return (255,150,0)
        elif color_index==2:
            return (235,190,2)
        elif color_index==3:
            return (215,40,1)
        elif color_index==4:
            return (255,150,0)
        elif color_index==5:
            return (235,190,2)
        elif color_index==6:
            return (215,40,1)
        elif color_index==7:
            return (255,200,200)
        return (255,150,0)

