import pygame.font
import game_functions as gf
import os

COLOR_INACTIVE = pygame.Color('lightskyblue3')
COLOR_ACTIVE = pygame.Color('dodgerblue2')

class Text_box():
    def __init__(self, ai_settings, screen, height_down=0,align_mode=1,text=''):
        """Initialize button attributes."""
        self.active=False
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.text=text
        self.bob=0
        #Set the dimensions and properties of the button.
        self.width, self.height = 200, 50
        if ai_settings.color_scheme==2:
            self.button_color = (200, 0, 0)
            self.text_color = (40,0,0)
        else:
            self.button_color = (0, 170, 0)
            self.text_color = (255,255,170)

        filename = "OpenSans-Regular.ttf"
        myfontfile = gf.resource_path(os.path.join("data", filename))
        self.font = pygame.font.Font(myfontfile, 36)
        
        #Build the button's rect object and center it.
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        if align_mode==0:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.left)
        elif align_mode==1:
            (self.rect.top,self.rect.centerx) = (self.screen_rect.top+height_down,self.screen_rect.centerx)
        
        self.prep_msg(self.text)

    def prep_msg(self, text):
        """Turn msg into a rendered image and center text on the button."""
        self.bob=0
        self.msg_image = self.font.render(text, True, self.text_color, self.button_color)
        width = max(200, self.msg_image.get_width()+10)
        self.rect.width = width
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        self.prep_msg(self.text)

        #Draw blank button and then draw message
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)
