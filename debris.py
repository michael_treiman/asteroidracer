import pygame
import math
import random
import game_functions as gf
from pygame.sprite import Sprite

class Debris(Sprite):
    """A class to manage debris from the destroyed ship."""

    def __init__(self, ai_settings,  screen, ship, poly_side):
        """Create a bullet object at the ship's current position."""
        super(Debris, self).__init__()
        self.screen = screen

        # Create debris. Debris is drawn at ship coords, unlike everything
        #else which is drawn at global coordinates.
        
        rotpoly=gf.rot_poly(ship.poly,0,0,ship.dir_facing)
        
        array_length=ship.poly_len-1
        #a, index of first point in the edge. b, index of second point
        a=poly_side
        b=poly_side+1
        if b>array_length:
            b=0
        pdx1 = rotpoly[a,0]
        pdx2 = rotpoly[b,0]
        pdy1 = rotpoly[a,1]
        pdy2 = rotpoly[b,1]
        self.x = ship.centerx+(pdx1+pdx2)/2
        self.y = ship.centery+(pdy1+pdy2)/2
        self.x_vel = ship.x_vel+random.gauss(0,.9)
        self.y_vel = ship.y_vel+random.gauss(0,.9)


        self.rot = math.atan2((pdy2-pdy1),(pdx2-pdx1))
        self.rot_v = random.gauss(0,.1)
        if ship.turning_left:
            self.rot_v-=.2
        if ship.turning_right:
            self.rot_v+=.2
        self.seg_length = math.sqrt((pdy2-pdy1)*(pdy2-pdy1)+(pdx2-pdx1)*(pdx2-pdx1))

        #Store the bullet's position as a decimal value
        self.lifetime = 0
        self.expired = False
        self.dietime = 35 + random.randrange(0,11)
        
        self.color_index= random.randrange(1,8)
        self.color = (255,150,0)
        self.color=self.getdebriscolorfromindex(self.color_index)
        self.speed_factor = ai_settings.bullet_speed_factor
    
    
        self.screen_rect = screen.get_rect()

    def update(self):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        self.x += self.x_vel
        self.y += self.y_vel
        #Update the rect position.
        self.rot += self.rot_v
        if self.lifetime>=self.dietime:
            self.expired=True
        self.lifetime = self.lifetime+1
    
    
    def draw_debris(self,screen,gameRect):
        """Draw the bullet to the screen."""
        c_r=math.cos(self.rot)
        s_r=math.sin(self.rot)
        s_x = self.x + c_r*self.seg_length/2
        s_y = self.y + s_r*self.seg_length/2
        e_x = self.x - c_r*self.seg_length/2
        e_y = self.y - s_r*self.seg_length/2
        
        pygame.draw.line(screen, (255,255,255), (s_x,s_y), (e_x,e_y), 3)
        #pygame.draw.line(screen, (255,255,255), (100,400), (500,1000), 1)
        #print("self.x,self.y,self.x,s_x,s_y,e_x,e_y,c_r,s_r: ",self.x,self.y,self.x,s_x,s_y,e_x,e_y,c_r,s_r)



    def getdebriscolorfromindex(self,color_index):
        if color_index==1:
            return (255,150,0)
        elif color_index==2:
            return (235,190,2)
        elif color_index==3:
            return (215,40,1)
        elif color_index==4:
            return (255,150,0)
        elif color_index==5:
            return (235,190,2)
        elif color_index==6:
            return (215,40,1)
        elif color_index==7:
            return (255,200,200)
        return (255,150,0)

