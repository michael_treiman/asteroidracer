import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Finish_line2(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, state, screen,line_start_x,line_start_y,line_end_x,line_end_y):
        """Create a bullet object at the ship's current position."""
        super(Finish_line2, self).__init__()
        self.screen = screen
                # Create a bullet rect at (0,0) and then set the correct position.
       
        #Store the bullet's position as a decimal value
        self.line_start_x=line_start_x
        self.line_start_y=line_start_y
        self.line_end_x=line_end_x
        self.line_end_y=line_end_y
        self.line_start_shifted_x=self.line_start_x+state.bgoffset_x
        self.line_start_shifted_y=self.line_start_y+state.bgoffset_y
        self.line_end_shifted_x=self.line_end_x+state.bgoffset_x
        self.line_end_shifted_y=self.line_end_y+state.bgoffset_y
        self.color_index=1
        self.color = (255,0,0)
        self.period = 15
    
    def update(self):
        """Nothing"""

    def draw_finish_line(self,gameRect,state):
        """Draw"""
        #offset factor determines how far away this layer is
        

        #if (gameRect.collidepoint(left,top) or gameRect.collidepoint(right,bottom)):
        phase=state.frames_elapsed%self.period
        
        if phase>6:
            self.color_index=0
            
        elif phase==0:
            self.color_index=1
        elif phase==1:
            self.color_index=2
        elif phase==2:
            self.color_index=3
        elif phase==3:
            self.color_index=4
        elif phase==4:
            self.color_index=3
        elif phase==5:
            self.color_index=2
        elif phase==6:
            self.color_index=1
        self.color=self.getcolorfromindex(self.color_index)
        bgoffset_x2=int(state.bgoffset_x)
        bgoffset_y2=int(state.bgoffset_y)
        self.line_start_shifted_x=self.line_start_x+bgoffset_x2
        self.line_start_shifted_y=self.line_start_y+bgoffset_y2
        self.line_end_shifted_x=self.line_end_x+bgoffset_x2
        self.line_end_shifted_y=self.line_end_y+bgoffset_y2
        
        pygame.draw.line(self.screen, self.color, [self.line_start_shifted_x,self.line_start_shifted_y],[self.line_end_shifted_x,self.line_end_shifted_y],5)
    
 
    def getcolorfromindex(self,color_index):
        if color_index==0:
            return (255,0,0)
        elif color_index==1:
            return (255,30,0)
        elif color_index==2:
            return (255,60,0)
        elif color_index==3:
            return (255,90,0)
        elif color_index==4:
            return (255,120,0)
        return (255,0,0)


