# -*- mode: python -*-

block_cipher = None


a = Analysis(['AsteroidRacer.py'],
             pathex=['/Users/michaeltreiman/TheBlackOfSpaceTestBranch'],
             binaries=[],
             datas=[('data/OpenSans-Regular.ttf','data'),('data/dont_stop_the_music.ogg','data')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='AsteroidRacer',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='AsteroidRacer')

app = BUNDLE(exe,
         name='AsteroidRacer.app',
         icon=None,
         bundle_identifier=None)
