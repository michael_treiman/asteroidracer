import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random
from state import State
import game_functions as gf


class Asteroid(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings,screen,spawn_rect):
        def map_probability(tuple,params=0):
            return tuple
        """Create a bullet object at the ship's current position."""
        super(Asteroid, self).__init__()
        self.screen = screen
        
        
        self.x = 300
        self.y = 400

        self.color = ai_settings.bullet_color
        self.speed_factor = ai_settings.bullet_speed_factor
        self.speed_spinning = .02
        
        self.dir_facing = 0
        self.dir_moving = 0
        self.speed = 0
        self.mass_base=100
        self.screen_rect = screen.get_rect()
    
        self.x_vel=0
        self.y_vel=0
        
        """self.poly = numpy.array([
        [6,20,1],
        [20,20,1],
        [20,8,1],
        [15,0,1],
        [3,-17,1],
        [-5,-25,1],
        [-14,-14,1],
        [-20,-6,1],
        [-13,1,1],
        [-3,6,1],
        [3,16,1],
        [6,20,1]])"""
        #random numbers

        a = random.randrange(-8,8)
        b = random.randrange(-8,8)
        c = random.randrange(-8,8)
        d = random.randrange(-8,8)
        e = random.randrange(-8,8)
        f = random.randrange(-8,8)
        g = random.randrange(-8,8)
        h = random.randrange(-8,8)
        i = random.randrange(-8,8)
        j = random.randrange(-8,8)
        k = random.randrange(-8,8)
        l = random.randrange(-8,8)
        m = random.randrange(-8,8)
        self.size_multiplier= random.gammavariate(1.5, 1)
        self.ci=random.randrange(2,5)
        
        if ai_settings.color_scheme==4:
            self.poly = numpy.array([
            [6.0+a,20.0+b,self.ci*10+random.randrange(1,10)],
            [20.0+a,20.0+d,self.ci*10+random.randrange(1,10)],
            [20.0+c,8.0,self.ci*10+random.randrange(1,10)],
            [15.0+e,0.0+b,self.ci*10+random.randrange(1,10)],
            [3.0+l,-17.0+f,self.ci*10+random.randrange(1,10)],
            [-5.0+c,-25.0+m,self.ci*10+random.randrange(1,10)],
            [-14.0+j,-14.0+h,self.ci*10+random.randrange(1,10)],
            [-20.0+k,-6.0+i,self.ci*10+random.randrange(1,10)],
            [-13.0+e,1.0+j,self.ci*10+random.randrange(1,10)],
            [-3.0+g,6.0+d,self.ci*10+random.randrange(1,10)],
            [3.0,16.0+f,self.ci*10+random.randrange(1,10)],
            [6.0+a,20.0+b,self.ci*10+random.randrange(1,10)]])
        else:
            self.poly = numpy.array([
            [6.0+a,20.0+b,self.ci],
            [20.0+a,20.0+d,self.ci],
            [20.0+c,8.0,self.ci],
            [15.0+e,0.0+b,self.ci],
            [3.0+l,-17.0+f,self.ci],
            [-5.0+c,-25.0+m,self.ci],
            [-14.0+j,-14.0+h,self.ci],
            [-20.0+k,-6.0+i,self.ci],
            [-13.0+e,1.0+j,self.ci],
            [-3.0+g,6.0+d,self.ci],
            [3.0,16.0+f,self.ci],
            [6.0+a,20.0+b,self.ci]])
        self.poly[:,0]*=self.size_multiplier
        self.poly[:,1]*=self.size_multiplier
        #rotpoly stores the rotated coordinates when the asteroid is on the screen
        self.starting_angle= random.uniform(0,3.1415926*2)
            
        self.poly=gf.rot_poly(self.poly,0,0,self.starting_angle)
        self.rotpoly=self.poly.copy()
        
        self.speed_spinning = random.gauss(0, .1)/self.size_multiplier
        self.period=int(2*3.1415926535/self.speed_spinning)
        self.period=10
        #self.angle_per_phase=2*3.1415926535/self.period
        self.angle_per_phase=1
        
        self.x = random.random()
        self.y = random.random()
        #print("x,y: ",self.x,self.y)
        self.x,self.y = map_probability((self.x,self.y))
        #print("x,y: ",self.x,self.y)
        self.x = int(spawn_rect.left + self.x*(spawn_rect.right-spawn_rect.left))
        self.y = int(spawn_rect.top + self.y*(spawn_rect.bottom-spawn_rect.top))
        #print("x,y: ",self.x,self.y)
        
        #self.x=random.randrange(spawn_rect.left,spawn_rect.right)
        #self.y=random.randrange(spawn_rect.top,spawn_rect.bottom)
        
        dx=self.x-ai_settings.ship_start_coords[0]
        
        dy=self.y-ai_settings.ship_start_coords[1]
        d2ship_spawn_sq=dx*dx+dy*dy
        if d2ship_spawn_sq<ai_settings.ship_start_cleared_rad*ai_settings.ship_start_cleared_rad:
            self.x=0
            self.y=0
        #print("ast x,y :",self.x,self.y)
        self.rect = pygame.Rect(self.x-35*self.size_multiplier, self.y-35*self.size_multiplier, 70*self.size_multiplier,70*self.size_multiplier )
    def update(self,state):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        #self.x += self.x_vel
        #self.y += self.y_vel
        #Update the rect position.
        #phase = state.frames_elapsed%self.period
        #self.rotpoly = gf.rot_poly(self.poly,0,0,phase*self.angle_per_phase)
        #self.dir_facing+=self.speed_spinning
        
    def draw_asteroid(self,gameRect,bgoffset_x,bgoffset_y,ai_settings):
        """Draw the bullet to the screen."""
        top=self.rect.top+bgoffset_y
        bottom=self.rect.bottom+bgoffset_y
        left=self.rect.left+bgoffset_x
        right=self.rect.right+bgoffset_x
        
        if (gameRect.collidepoint(left,top) or gameRect.collidepoint(left,bottom) or gameRect.collidepoint(right,top) or gameRect.collidepoint(right,bottom)):
            #print("Ast ",top,left,bottom,right)
            #print(gameRect)
            #game_functions.draw_poly(self.screen,self.poly,self.x+bgoffset_x, self.y+bgoffset_y,self.dir_facing,ai_settings)
            gf.draw_poly(self.screen,self.poly,self.x+bgoffset_x, self.y+bgoffset_y,self.dir_facing,ai_settings)

            #def getcolorfromindex(color_index):


