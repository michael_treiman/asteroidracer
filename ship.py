import pygame
import math
import numpy
import game_functions as gf
from pygame.sprite import Group
from debris import Debris

class Ship():

    def __init__(self, ai_settings, screen,id=0):
        """Initialize the ship and set its starting position."""
        self.id=id
        self.tracer_active=True
        self.screen = screen
        self.ai_settings = ai_settings
        # Load the ship image and get its rect.
        self.rect=pygame.Rect(0,0,25,25)
        self.screen_rect = screen.get_rect()
        self.restart(ai_settings)
        #self.poly = numpy.array([[0,20,1],[20,20,1],[20,0,1],[0,0,1],[0,20,1]])
        """self.poly = numpy.array([
        [6,20,1],
        [20,20,1],
        [20,8,1],
        [15,0,1],
        [3,-17,1],
        [-5,-25,1],
        [-14,-14,1],
        [-20,-6,1],
        [-13,1,1],
        [-3,6,1],
        [3,16,1],
        [6,20,1]])"""
    
        self.poly = numpy.array([
        [0,15,1],
        [4,-10,1],
        [-4,-10,1],
        [0,15,1]])

        self.poly_len=len(self.poly)
        self.debrises = Group()
        for i in range (1,self.poly_len):
            new_debris = Debris(ai_settings,  screen, self, i)
            self.debrises.add(new_debris)
        
    def update(self,state,ai_settings,screen):
        if self.id==1:
            self.is_Destroyed=state.ship_blue_destroyed
        else:
            self.is_Destroyed=state.ship_destroyed
        
        if not self.is_Destroyed and self.damage>100:
        
            if self.id==1:
               state.ship_blue_destroyed=True
            else:
                state.ship_destroyed=True



            
            
            self.is_Destroyed=True
            
            
            print("The ship has been destroyed")

            for i in range (0,self.poly_len-1):
                new_debris = Debris(ai_settings, screen, self, i)
                self.debrises.add(new_debris)
            self.x_vel=0
            self.y_vel=0
            self.heading_speed=0
        
        elif self.is_Destroyed:
            self.debrises.update()
            self.dead_counter+=1
            if self.dead_counter > ai_settings.ship_respawn_frames:
                self.damage=0
                self.dead_counter=0
                self.is_Destroyed=False
                if self.id==1:
                    state.ship_blue_destroyed=False
                else:
                    state.ship_destroyed=False
                self.debrises.empty()

        """This is the realistic thrust model."""
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.center += self.ai_settings.ship_speed_factor
  
        if self.moving_left and self.rect.left > self.screen_rect.left:
            self.center -= self.ai_settings.ship_speed_factor

        if self.boosting and not self.is_Destroyed and not state.thrust_frozen:
            #print("The ship is boosting.")
            self.x_vel+=self.a_x
            self.y_vel+=self.a_y
            self.heat+=4
            #update heading and speed, which is used in collisions
            self.update_heading()
            """self.heading_speed=math.sqrt(self.x_vel*self.x_vel+self.y_vel*self.y_vel)
            self.heading_angle=math.atan2(self.y_vel,self.x_vel)"""
            

        if self.turning_left:
            #print("The ship is turning left.")
            self.dir_facing += .2
            self.a_x=math.sin(self.dir_facing)*.1
            self.a_y=math.cos(self.dir_facing)*.1
            #self.turn_speed+=.002
        if self.turning_right:
            #print("The ship is turning right.")
            #self.turn_speed-=.002
            self.dir_facing -= .2
            self.a_x=math.sin(self.dir_facing)*.1
            self.a_y=math.cos(self.dir_facing)*.1
        #if self.turning_right or self.turning_left or self.boosting:
            #print("angle: ", self.dir_facing, "x vel: ", self.x_vel, "y vel: ", self.y_vel)
        if self.col_timer>0:
            self.col_timer-=1
   
        
        self.x_nib = math.sin(self.dir_facing) * 40
        self.y_nib = math.cos(self.dir_facing) * 40
            
        self.heat-=2
        if self.heat>255:
            self.heat=255
        if self.heat<0:
            self.heat=0
        self.centerx+=self.x_vel
        self.centery+=self.y_vel


        self.rect.centerx=self.centerx
        self.rect.centery=self.centery
        
        if (self.id==0 and state.frames_elapsed%20==1):
            ship_game_x,ship_game_y=self.centerx-state.bgoffset_x,self.centery-state.bgoffset_y
            print("[", int(ship_game_x),",",int(ship_game_y),"],")


    def blitme(self,state,ai_settings,screen,gameRect):
        color = int(max(min(self.heat,255),0))
        """Draw the ship at its current location."""

        if self.is_Destroyed:
            for debris in self.debrises.sprites():
                debris.draw_debris(screen,gameRect)
        else:
            gf.draw_poly(screen,self.poly,self.centerx, self.centery,self.dir_facing,ai_settings)
            #pygame.draw.line(screen, (0,240,240), [self.centerx, self.centery], [self.centerx+self.x_nib, self.centery+self.y_nib], 3)
            #pygame.draw.circle(screen, (color,color,color), [int(self.centerx), int(self.centery)], 8, 0)
            if self.tracer_active:
                self.draw_tracer(screen,ai_settings)
    

    def draw_tracer(self,screen,ai_settings):
        #tracer_x=(self.x_vel+math.sin(self.dir_facing)*3)*100
        #tracer_y=(self.y_vel+math.cos(self.dir_facing)*3)*100
        if ai_settings.s_tracer_active:
            #Standard Tracer shows where the ship is going
            tracer_x=(self.x_vel)*100
            tracer_y=(self.y_vel)*100
            pygame.draw.line(screen, (50,50,255), [self.centerx, self.centery], [self.centerx+tracer_x, self.centery+tracer_y], 2)
        #stop range tracer
        if ai_settings.sr_tracer_active:


            #Stop range tracer shows how far it would take for the ship to
            #stop if it boosted in the opposite direction of its motion
            stop_range=self.heading_speed*self.heading_speed/(2*.1)
            stop_range_x=self.x_vel*self.x_vel/(2*.1)
            stop_range_y=self.x_vel*self.x_vel/(2*.1)
            sr_tracer_cx= stop_range*math.cos(self.heading_angle)
            sr_tracer_cy= stop_range*math.sin(self.heading_angle)
            sr_tracer_crossx = sr_tracer_cy/8
            sr_tracer_crossy = -sr_tracer_cx/8
            sr_tracer_cx+=+self.centerx
            sr_tracer_cy+=+self.centery
            pygame.draw.line(screen, (50,50,255), [sr_tracer_cx+sr_tracer_crossx, sr_tracer_cy+sr_tracer_crossy], [sr_tracer_cx-sr_tracer_crossx, sr_tracer_cy-sr_tracer_crossy], 3)
    
        #Parabolic tracer shows what path the ship would follow if it
        #boosted on its current facing direction
        if ai_settings.p_tracer_active:
            #parabolic tracer (where the ship will go if you boost)s

            t1=7
            t2=14
            t3=21
            t4=28
            t5=35
            x1 = self.parabola_intersect(t1,self.centerx,self.x_vel,self.a_x)
            x2 = self.parabola_intersect(t2,self.centerx,self.x_vel,self.a_x)
            x3 = self.parabola_intersect(t3,self.centerx,self.x_vel,self.a_x)
            x4 = self.parabola_intersect(t4,self.centerx,self.x_vel,self.a_x)
            x5 = self.parabola_intersect(t5,self.centerx,self.x_vel,self.a_x)
        
            y1 = self.parabola_intersect(t1,self.centery,self.y_vel,self.a_y)
            y2 = self.parabola_intersect(t2,self.centery,self.y_vel,self.a_y)
            y3 = self.parabola_intersect(t3,self.centery,self.y_vel,self.a_y)
            y4 = self.parabola_intersect(t4,self.centery,self.y_vel,self.a_y)
            y5 = self.parabola_intersect(t5,self.centery,self.y_vel,self.a_y)
        
            pygame.draw.line(screen, (180,180,255), [self.centerx,self.centery], [x1,y1], 2)
            pygame.draw.line(screen, (180,180,255), [x1,y1], [x2,y2], 2)
            pygame.draw.line(screen, (180,180,255), [x2,y2], [x3,y3], 2)
            pygame.draw.line(screen, (180,180,255), [x3,y3], [x4,y4], 2)
            pygame.draw.line(screen, (180,180,255), [x4,y4], [x5,y5], 2)
    def parabola_intersect(self,t,s0,v0,a):
    
        return s0+v0*t+a*t*t/2
    
    

    def recoil(self):
        self.x_vel -= math.sin(self.dir_facing)*.1
        self.y_vel -= math.cos(self.dir_facing)*.1
        self.heat+=80

    def update_heading(self):
        self.heading_speed=math.sqrt(self.x_vel*self.x_vel+self.y_vel*self.y_vel)
        self.heading_angle=math.atan2(self.y_vel,self.x_vel)

    def restart(self,ai_settings):
        self.is_Destroyed = False
        self.col_timer = 0
        #Start at bottom center
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        self.dead_counter = 0
        
        self.center = float(self.rect.centerx)
        (self.centerx,self.centery) = ai_settings.ship_start_coords
        #Movement flag
        self.moving_right = False
        self.moving_left = False
        
        self.turning_right = False
        self.turning_left = False
        self.boosting = False
        
        self.fuel = 100
        self.thruster_fuel = 100
        self.torpedos = 10
        self.turn_speed = 0
        self.dir_facing = 0
        self.dir_moving = 0
        #a_x and a_y are the acceleration the ship would experience at
        #that angle of thrust. These are only updated when the ship turns
        #this reduces the need for trig calculations.
        self.a_x=math.sin(self.dir_facing)*.1
        self.a_y=math.cos(self.dir_facing)*.1
        self.speed = 0
        self.mass_base=100
        self.mass_per_fuel=.1
        self.mass_per_torpedo=.1
        self.x_vel = 0
        self.y_vel = 0

        self.x_nib = math.sin(self.dir_facing) * 20
        self.y_nib = math.cos(self.dir_facing) * 20
        self.heat=0
        self.delaythrustx=0
        self.delaythrusty=0
        self.heading_angle=0
        self.heading_speed=0
        self.damage=0


