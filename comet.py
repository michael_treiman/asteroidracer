import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Comet(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen, spawn_rect):
        """Create a bullet object at the ship's current position."""
        super(Comet, self).__init__()
        self.screen = screen
        """ spawnrect_temp=spawnrect.copy()
        spawnrect_temp.width*=ai_settings.closest_bg_layer_factor
        spawnrect_temp.height*=ai_settings.closest_bg_layer_factor"""
        # Create a bullet rect at (0,0) and then set the correct position.
       
        #Store the bullet's position as a decimal value
        self.color_index=random.randrange(1,4)
        self.line_color=self.getlinecolorfromindex(ai_settings,self.color_index)
        self.ball_color=self.getballcolorfromindex(ai_settings,self.color_index)
        self.x = 300
        self.y = 400
        
        self.offsetfactor=random.randrange(2,6)
        self.tail_width=int(10/self.offsetfactor)
        self.color = ai_settings.bullet_color
        self.speed_factor = ai_settings.bullet_speed_factor
        self.speed_spinning = .02
        self.dir_facing = 0
        self.dir_moving = random.uniform(0,3.1415926*2)
        self.tail_dir = ai_settings.comet_tail_dir
        self.tail_length = random.randrange(300,600)/self.offsetfactor
        self.speed = random.gauss(0, 4)/self.offsetfactor
        self.mass_base=100
        self.screen_rect = screen.get_rect()
        self.radius = int(random.randrange(10,20)/self.offsetfactor)
        self.x_vel= self.speed * math.sin(self.dir_moving)
        self.y_vel= self.speed * math.cos(self.dir_moving)
        

        self.speed_spinning = random.gauss(0, .01)
        self.x=random.randrange(spawn_rect.left,spawn_rect.right)
        self.y=random.randrange(spawn_rect.top,spawn_rect.bottom)
        self.tail_x=self.tail_length * math.sin(self.tail_dir)
        self.tail_y=self.tail_length * math.cos(self.tail_dir)
        self.int_tail_x=int(self.tail_x)
        self.int_tail_y=int(self.tail_y)
    
    def update(self):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        self.x += self.x_vel
        self.y += self.y_vel
        #Update the rect position.

        #self.dir_facing+=self.speed_spinning
        
    def draw_comet(self,gameRect,bgoffset_x,bgoffset_y):
        """Draw the bullet to the screen."""
        #offset factor determines how far away this layer is
        
        
        bgoffset_x2=int(bgoffset_x/self.offsetfactor)
        bgoffset_y2=int(bgoffset_y/self.offsetfactor)
        
        self.int_x=int(self.x)
        self.int_y=int(self.y)
        
        self.xoffset=self.int_x+bgoffset_x2
        self.yoffset=self.int_y+bgoffset_y2
        
        self.tail_end_x=self.xoffset+self.int_tail_x
        self.tail_end_y=self.yoffset+self.int_tail_y
        left=self.xoffset
        top=self.yoffset
        right=self.tail_end_x
        bottom=self.tail_end_y

        if (gameRect.collidepoint(left,top) or gameRect.collidepoint(left,bottom) or gameRect.collidepoint(right,top) or gameRect.collidepoint(right,bottom)):
            #self.xoffset=self.int_x+bgoffset_x
            #self.yoffset=self.int_y+bgoffset_y
            pygame.draw.line(self.screen, self.line_color, [self.xoffset, self.yoffset],
            [self.xoffset+self.int_tail_x, self.yoffset+self.int_tail_y], self.tail_width)
            pygame.draw.circle(self.screen,self.ball_color,[self.xoffset, self.yoffset],self.radius)
    
    def getlinecolorfromindex(self,ai_settings,color_index=1):
        if ai_settings.color_scheme==2:
            if color_index==1:
                return (250,250,250)
            elif color_index==2:
                return (195,0,0)
            elif color_index==3:
                return (210,210,255)
                
        if color_index==1:
            return (0,0,255)
        elif color_index==2:
            return (0,245,180)
        elif color_index==3:
            return (170,40,230)
        return (0,0,255)

    def getballcolorfromindex(self,ai_settings,color_index=1):
        if ai_settings.color_scheme==2:
            if color_index==1:
                return (255,255,255)
            elif color_index==2:
                return (245,0,0)
            elif color_index==3:
                return (220,220,255)
        if color_index==1:
            return (80,80,255)
        elif color_index==2:
            return (80,255,220)
        elif color_index==3:
            return (200,80,255)
        return (80,80,255)


