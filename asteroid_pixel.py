import pygame
import math
from pygame.sprite import Sprite
from pygame.sprite import Group
from asteroid import Asteroid
from pulsar import Pulsar
import game_functions
import numpy
import random
from state import State
import game_functions as gf


class AsteroidPixel(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self,i,j,count,typevar,ai_settings,screen):
        """Create a bullet object at the ship's current position."""
        super(AsteroidPixel, self).__init__()
        self.screen = screen
        #the random seed is appended with i,j for every pixel so the
        #asteroid positions are always different
        random.seed(typevar+str(i)+","+str(j))

        self.i=i
        self.j=j
        
        left_top = (i*1000,j*1000)
        width_height = (1000,1000)
        
        
        self.asteroids = Group()
        self.spawn_rect=pygame.Rect(left_top, width_height)
        self.count=int(random.uniform(0,ai_settings.ast_grid_count))
        for i in range (1,self.count):
            new_asteroid = Asteroid(ai_settings,screen,self.spawn_rect)
            self.asteroids.add(new_asteroid)

        self.pulsars = Group()
        self.p_count= ai_settings.num_pulsars
        for i in range (1,ai_settings.num_pulsars):
            new_pulsar = Pulsar(ai_settings, screen,self.spawn_rect)
            self.pulsars.add(new_pulsar)
        

    def update(self,state):
        """Placeholder method. Later it may be used to move the asteroids"""

            



