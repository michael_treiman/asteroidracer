import tkinter as tk                # python 3
from tkinter import *
from tkinter import messagebox
import sys
from tkinter import font  as tkfont # python 3
from PIL import ImageTk, Image

import pygame
import time
from pygame.sprite import Group
import game_functions as gf
from button2 import Button2
from state import State
from settings import Settings
from ship import Ship
from asteroid import Asteroid
from asteroid_pixel import AsteroidPixel
from starfield import Starfield
from comet import Comet
from planet import Planet
from pulsar import Pulsar
from timer import Timer
from finish_line import Finish_line
from text_box import Text_box
from text2 import Text2
from game_config import Game_config
import TheBlackOfSpace
import urllib.request

def client_exit():
    print("The exit button was pushed")
    sys.exit()

#def enter_game(str=""):


class SampleApp(tk.Tk):


    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        state=State()
        settings=Settings()

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, PageOne, PageTwo, PageThree, PageFour, PageFive, PageSix, PageSeven, PageEight, PageNine, PageTen, PageEleven, PageTwelve, PageThirteen, PageFourteen, PageFifteen, PageSixteen):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()

    def enter_game(self):

        TheBlackOfSpace.run_game()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Welcome to Asteroid Racer", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        path = "title.gif"
        photo = PhotoImage(file=path)
        label = Label(image=photo)
        label.image = photo # keep a reference!
        label.pack()

        button1 = tk.Button(self, text="Story Mode", command=lambda: controller.show_frame("PageOne"))
        #button2 = tk.Button(self, text="Time Trial", command=lambda: controller.show_frame("PageTwo"))
        button2 = tk.Button(self, text="Time Trial", command=lambda: controller.enter_game())
        button2 = tk.Button(self, text="Time Trial", command= controller.enter_game)
        button3 = tk.Button(self, text="Explore", command=lambda: controller.show_frame("PageThree"))
        button4 = tk.Button(self, text="Options", command=lambda: controller.show_frame("PageTen"))
        button5 = tk.Button(self, text="Credits", command=lambda: controller.show_frame("PageEleven"))
        button6 = tk.Button(self, text="Quit", command=client_exit)
        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()
        button6.pack()




class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Story mode: Choose a stage", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Hangar",
                           command=lambda: controller.show_frame("PageFive"))
        button2 = tk.Button(self, text="Space Bar",
                           command=lambda: controller.show_frame("PageTwelve"))
        button3 = tk.Button(self, text="Wreckage",
                           command=lambda: controller.show_frame("PageThirteen"))
        button4 = tk.Button(self, text="Moons",
                           command=lambda: controller.show_frame("PageFourteen"))
        button5 = tk.Button(self, text="Evasion",
                           command=lambda: controller.show_frame("PageFifteen"))
        button6 = tk.Button(self, text="Nebula",
                           command=lambda: controller.show_frame("PageSixteen"))
        button7 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))
                           
                           

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()
        button6.pack()
        button7.pack()



class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Time trial: Choose a stage", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Hangar",
                           command=lambda: controller.show_frame("StartPage"))
        button2 = tk.Button(self, text="Space Bar",
                           command=lambda: controller.show_frame("StartPage"))
        button3 = tk.Button(self, text="Wreckage",
                           command=lambda: controller.show_frame("StartPage"))
        button4 = tk.Button(self, text="Moons",
                           command=lambda: controller.show_frame("StartPage"))
        button5 = tk.Button(self, text="Evasion",
                           command=lambda: controller.show_frame("StartPage"))
        button6 = tk.Button(self, text="Nebula",
                           command=lambda: controller.show_frame("StartPage"))
        button7 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()
        button6.pack()
        button7.pack()


class PageThree(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Exploration: Choose a stage", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Hangar",
                           command=lambda: controller.show_frame("StartPage"))
        button2 = tk.Button(self, text="Space Bar",
                           command=lambda: controller.show_frame("StartPage"))
        button3 = tk.Button(self, text="Wreckage",
                           command=lambda: controller.show_frame("StartPage"))
        button4 = tk.Button(self, text="Moons",
                           command=lambda: controller.show_frame("StartPage"))
        button5 = tk.Button(self, text="Evasion",
                           command=lambda: controller.show_frame("StartPage"))
        button6 = tk.Button(self, text="Nebula",
                           command=lambda: controller.show_frame("StartPage"))
        button7 = tk.Button(self, text="Custom Stage",
                           command=lambda: controller.show_frame("StartPage"))
        button8 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()
        button6.pack()
        button7.pack()
        button8.pack()

class PageFour(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Options", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Music",
                           command=lambda: controller.show_frame("StartPage"))

        button1.pack()

class PageFive(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Hangar Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Sterling McGovern - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="Zaphod Beeblebrox - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="Grease Wingstream - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="Zebulon Hazard - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()



class PageSix(tk.Frame):


    
    def __init__(self, parent, controller):
        def next_opp_msg(*args):
            taunt_index.set(taunt_index.get()+1)
            print(taunt_index.get())
            if taunt_index.get()==len(taunt_str)-1:
                NextButton.config(text="Race")
            elif taunt_index.get()>=len(taunt_str):
                NextButton.config(text="Next")
                #oppTauntStr.set("")
                #set_story_mode()
            oppTauntStr.set(taunt_str[taunt_index.get()])
        i=0
        taunt_index=IntVar()
        taunt_index.set(0)
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Sterling McGovern", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
        taunt_str= [
        "I don't think I've seen you around here. Are you new?",
        "My name's Sterling McGovern. I come here to hang out with the boys and watch some asteroid races.",
        "Sometimes I like to try my hand at a race. These small craft aren't my specialty, though.",
        "In charge of one of the big tankers, I would smoke all of these people.",
        "What do you say, want to go?"]
        
        NextButton = tk.Button(self, text="Next",
                           command=next_opp_msg)
        button2 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageFive"))
        
        NextButton.pack()
        button2.pack()

        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( self, textvariable=oppTauntStr )
        oppTauntMsg.pack()

class PageSeven(tk.Frame):



    def __init__(self, parent, controller):
        def next_opp_msg(*args):
            taunt_index.set(taunt_index.get()+1)
            print(taunt_index.get())
            if taunt_index.get()==len(taunt_str)-1:
                NextButton.config(text="Race")
            elif taunt_index.get()>=len(taunt_str):
                NextButton.config(text="Next")
                #oppTauntStr.set("")
                #set_story_mode()
            oppTauntStr.set(taunt_str[taunt_index.get()])
        i=0
        taunt_index=IntVar()
        taunt_index.set(0)
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Zaphod Beeblebrox", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
        taunt_str= [
        "Howdy, friend. Do you like my sunglasses? Pretty fashionable, huh?",
        "I'm Zaphod Beeblebrox, former President of the galaxy. You may have heard of me.",
        "No? You must just be living under a rock.",
        "After retiring from public service I decided I'd give back to the community.",
        "Unfortunately, that was dreadfully dull. So instead I took up asteroid racing."
        ]
        NextButton = tk.Button(self, text="Next",
                           command=next_opp_msg)
        button2 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageFive"))
        
        NextButton.pack()
        button2.pack()

        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( self, textvariable=oppTauntStr )
        oppTauntMsg.pack()

class PageEight(tk.Frame):



    def __init__(self, parent, controller):
        def next_opp_msg(*args):
            taunt_index.set(taunt_index.get()+1)
            print(taunt_index.get())
            if taunt_index.get()==len(taunt_str)-1:
                NextButton.config(text="Race")
            elif taunt_index.get()>=len(taunt_str):
                NextButton.config(text="Next")
                #oppTauntStr.set("")
                #set_story_mode()
            oppTauntStr.set(taunt_str[taunt_index.get()])
        i=0
        taunt_index=IntVar()
        taunt_index.set(0)
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Grease Wingstream", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
        taunt_str= [
        "My ship is quite a beauty, isn't it? It always attracts the eye of passersby.",
        "I have put a lot of work into it. Is that your ship?",
        "Oh, you might want to spend some time cleaning it up.",
        "But hey, looks aren't everything, right? I'm Grease Wingstream, former pilot of the Galactic Navy.",
        "What, you really want to race? You think a rockhopper like you can keep up? I've got military training!",
        "Don't feel bad when you lose. Just don't do anything stupid."

        ]
        NextButton = tk.Button(self, text="Next",
                           command=next_opp_msg)
        button2 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageFive"))
        
        NextButton.pack()
        button2.pack()

        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( self, textvariable=oppTauntStr )
        oppTauntMsg.pack()

class PageNine(tk.Frame):



    def __init__(self, parent, controller):
        def next_opp_msg(*args):
            taunt_index.set(taunt_index.get()+1)
            print(taunt_index.get())
            if taunt_index.get()==len(taunt_str)-1:
                NextButton.config(text="Race")
            elif taunt_index.get()>=len(taunt_str):
                NextButton.config(text="Next")
                #oppTauntStr.set("")
                #set_story_mode()
            oppTauntStr.set(taunt_str[taunt_index.get()])
        i=0
        taunt_index=IntVar()
        taunt_index.set(0)
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Zebulon Hazard", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
        taunt_str= [
        "Racing is an artform. We express ourselves through motion.",
        "I am always looking for the perfect race - one that melds man, machine, and nature.",
        "Sadly, the people in this circuit seem to only have textbook skills. No creativity!",
        "It is good to see another connoisseur. I can always tell."
        "Why don't you show me your flying artistry?"
        ]
        NextButton = tk.Button(self, text="Next",
                           command=next_opp_msg)
        button2 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageFive"))
        
        NextButton.pack()
        button2.pack()

        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( self, textvariable=oppTauntStr )
        oppTauntMsg.pack()


class PageTen(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        musicVar = IntVar()
        
        label = tk.Label(self, text="Options", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))
        Checkbutton(self, text="Play Music", variable= musicVar).pack()
        
        w = Scale(self, from_=0,to=11, orient=HORIZONTAL)
        w.pack()
        button1.pack()

class PageEleven(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        
        
        label = tk.Label(self, text="Options", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        
        label = tk.Label(self, text="""
                
                Programming: Michael Treiman \n
                Graphics: Michael Treiman \n
                Story: Michael Treiman \n
                Music: Bandara \n
                Sound Effects: Steven Montez \n
                Character Design and Art: Random person on Craigslist \n
                Programming Assistance: Nick Ashcroft \n
                Additional Assistance: Nayr Savid, Lewis Chiang, Ciaran Ryan-Andersen, Preston Crollett \n
                
                """, font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

        button1 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))

        button1.pack()


class PageTwelve(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Space Bar Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="sb1 - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="sb2 - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="sb3 - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="sb4 - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()

class PageThirteen(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Wreckage Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="w1 - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="w2 - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="w3 - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="w4 - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()

class PageFourteen(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Moons Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="m1 - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="m2 - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="m3 - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="m4 - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()


class PageFifteen(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Evasion Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="e1 - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="e2 - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="e3 - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="e4 - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()

class PageSixteen(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Nebula Opponents", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button1 = tk.Button(self, text="n1 - ★",
                           command=lambda: controller.show_frame("PageSix"))
        button2 = tk.Button(self, text="n2 - ★★",
                           command=lambda: controller.show_frame("PageSeven"))
        button3 = tk.Button(self, text="n3 - ★★★",
                           command=lambda: controller.show_frame("PageEight"))
        button4 = tk.Button(self, text="n4 - ★★★★",
                           command=lambda: controller.show_frame("PageNine"))
        button5 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("PageOne"))
 

        button1.pack()
        button2.pack()
        button3.pack()
        button4.pack()
        button5.pack()

if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()
