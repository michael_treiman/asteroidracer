import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random
import game_functions as gf

class Course(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, state, screen,id):
        """Create a bullet object at the ship's current position."""
        super(Course, self).__init__()
        self.screen = screen
    
    self.number_of_laps
    self.
    def draw_guide(self,gameRect,state,ai_settings):
        """Draw"""
        #offset factor determines how far away this layer is
        

        #if (gameRect.collidepoint(left,top) or gameRect.collidepoint(right,bottom)):
        phase=state.frames_elapsed%self.period
        
        if phase>6:
            self.color_index=0
            
        elif phase==0:
            self.color_index=1
        elif phase==1:
            self.color_index=2
        elif phase==2:
            self.color_index=3
        elif phase==3:
            self.color_index=4
        elif phase==4:
            self.color_index=3
        elif phase==5:
            self.color_index=2
        elif phase==6:
            self.color_index=1
        self.color=self.getcolorfromindex(self.color_index)
        bgoffset_x2=int(state.bgoffset_x)
        bgoffset_y2=int(state.bgoffset_y)
        

        gf.draw_poly(self.screen,self.poly,bgoffset_x2, bgoffset_y2,0,ai_settings)
     
    
 
    def getcolorfromindex(self,color_index):
        if color_index==0:
            return (255,0,0)
        elif color_index==1:
            return (255,30,0)
        elif color_index==2:
            return (255,60,0)
        elif color_index==3:
            return (255,90,0)
        elif color_index==4:
            return (255,120,0)
        return (255,0,0)


