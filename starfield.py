import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Starfield(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, i,j,count,typevar,layer,ai_settings):
        """Create a bullet object at the ship's current position."""
        
        #the pixels in extra border (e_b) ensure that stars are not cut off at the pixel seams
        self.e_b=50
        
        self.e_b2=2*self.e_b
        self.layer=layer
        self.i=i
        self.j=j
        super(Starfield, self).__init__()
        self.width=ai_settings.starfield_px_wd
        self.height=ai_settings.starfield_px_ht
        self.star_screen = pygame.Surface((self.width+self.e_b2,self.height+self.e_b2),pygame.SRCALPHA)
        self.star_screen.convert_alpha()
        self.star_screen.fill((0,0,0,0))
        ###################

        random.seed(typevar+str(layer)+","+str(i)+","+str(j))

        self.star_count=count//self.layer
        self.starfield = numpy.empty([self.star_count,6])
        for k in range (0,self.star_count):
        
            self.starfield[k,0] = math.floor(random.randint(0,self.width))
            
            self.starfield[k,1] = math.floor(random.randint(0,self.height))
            
            #self.starfield[k,2] = math.floor(random.expovariate(1)*5);
            self.starfield[k,2] = math.floor(random.gammavariate(1.3,1)*3);
            if (self.starfield[k,2]>100):
                self.starfield[k,2]=100
            if (self.starfield[k,2]<1):
                self.starfield[k,2]=1
            self.starfield[k,2]*=10/self.layer
            if ai_settings.color_scheme==2:
                a=math.floor(random.randint(210,255))
                b=math.floor(random.randint(50,170))
                
                self.starfield[k,3] = a
                self.starfield[k,4] = b
                self.starfield[k,5] = b
            
            elif ai_settings.color_scheme==3:
                a=math.floor(random.randint(0,120))
                b=math.floor(random.randint(0,80))
                
                self.starfield[k,3] = 5
                self.starfield[k,4] = a
                self.starfield[k,5] = min(255,a+b+40)
            
            else:
                self.starfield[k,3] = math.floor(random.randint(50,255))
                self.starfield[k,4] = math.floor(random.randint(50,255))
                self.starfield[k,5] = math.floor(random.randint(50,255))
            #print(self.screen_rect.width)
            #print(self.screen_rect.height)
    
        
    def render_starfield(self):
        """Draw the bullet to the screen."""
        length = len(self.starfield)
        for i in range (0,length):
            x = int(self.starfield[i,0])
            y = int(self.starfield[i,1])
            r = int(self.starfield[i,2])
            red = int(self.starfield[i,3])
            green = int(self.starfield[i,4])
            blue = int(self.starfield[i,5])
            
            pygame.draw.circle(self.star_screen,[red,green,blue],[x+self.e_b, y+self.e_b],r)
        self.mask=pygame.mask.from_threshold(self.star_screen,(0,0,0,0),(30,30,30,255))

    def draw_starfield(self,state,ai_settings,screen):
        
        star_left = math.floor(self.i*self.width)
        star_top = math.floor(self.j*self.height)
        screen_left = math.floor(-state.bgoffset_x/self.layer)
        screen_top = math.floor(-state.bgoffset_y/self.layer)
        
        screen.blit(self.star_screen,(star_left-screen_left-self.e_b,star_top-screen_top-self.e_b))

