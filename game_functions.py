import sys
import os
import pygame
from bullet import Bullet
from charge import Charge
from exhaust import Exhaust
from pulsar import Pulsar
import math
import numpy
import shelve


from state import State

def check_events(state,ai_settings, screen,tc,gc):
    p=True
    str=""
    """Respond to keypresses and mouse events."""
    for event in pygame.event.get():
        if(event.type == pygame.KEYDOWN) or (event.type == pygame.KEYUP):
            p=True
            if event.type ==2 and  event.key ==97:
                str="pev2_97"
            elif event.type ==3 and  event.key ==97:
                str="pev3_97"
            elif event.type ==2 and  event.key ==100:
                str="pev2_100"
            elif event.type ==3 and  event.key ==100:
                str="pev3_100"
            elif event.type ==2 and  event.key ==119:
                str="pev2_119"
            elif event.type ==3 and  event.key ==119:
                str="pev3_119"
            else:
                p=False
            #print("(",state.frames_elapsed,",",str,"),")
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                gc.ship.moving_right = True
            elif event.key == pygame.K_LEFT:
                gc.ship.moving_left = True
            elif event.key == pygame.K_a:
                gc.ship.turning_left = True
            elif event.key == pygame.K_d:
                gc.ship.turning_right = True
            elif event.key == pygame.K_w:
                gc.ship.boosting = True
            elif event.key == pygame.K_t:
                gc.ship.tracer_active = not gc.ship.tracer_active
            elif event.key == pygame.K_SPACE:
                if not state.ship_destroyed:
                    fire_bullet(ai_settings,screen,gc.ship,gc.bullets,state.bgoffset_x,state.bgoffset_y)
                    
                    
            elif event.key == pygame.K_u:
                set_scroll_top(ai_settings)
            elif event.key == pygame.K_h:
                set_scroll_left(ai_settings)
            elif event.key == pygame.K_n:
                set_scroll_bottom(ai_settings)
            elif event.key == pygame.K_j:
                set_scroll_right(ai_settings)
                
            #elif event.key == pygame.K_c:
            #    if not state.ship_destroyed:
            #        fire_charge(ai_settings,screen,gc.ship,gc.charges)
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                gc.ship.moving_right = False
            elif event.key == pygame.K_LEFT:
                gc.ship.moving_left = False
            elif event.key == pygame.K_a:
                gc.ship.turning_left = False
            elif event.key == pygame.K_d:
                gc.ship.turning_right = False
            elif event.key == pygame.K_w:
                gc.ship.boosting = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            mouse_game_x,mouse_game_y=mouse_x-state.bgoffset_x,mouse_y-state.bgoffset_y
            print("x,y=", mouse_game_x,mouse_game_y)
            if check_button(state,tc.menu_button,mouse_x,mouse_y):
                state.game_Active=False
                state.menu_Active=True
                state.window_active=False
                

            if check_button(state,tc.retry_button,mouse_x,mouse_y):
                gc.ship.restart(ai_settings)
                state.bgoffset_y=ai_settings.init_bgoffset_y
                state.bgoffset_x = 0
                gc.ship_blue.restart(ai_settings)
                state.frames_elapsed=0
                state.thrust_frozen=True

    #Do the event collection again for the "blue ship"
    #which is an opponent or a mirror of your previous run
    #print(blue_events)
    if state.story_mode:
        for blue_event in gc.blue_event:
            """print(blue_event[1])
            
            print(blue_event[1]=="pev2_100")
            print(blue_event[1]=="pev3_100")
            print(blue_event[1]=="pev2_119")
            print(blue_event[1]=="pev3_119")
            print(blue_event[1]=="pev2_97")
            print(blue_event[1]=="pev3_97")"""
            #print(blue_event)
            if blue_event[0]!=state.frames_elapsed:
                event=pygame.event.Event(3,key=99)
                #print("bobs")
            elif blue_event[1]=="pev2_119":
                event=pygame.event.Event(2,key=119)
            elif blue_event[1]=="pev3_119":
                event=pygame.event.Event(3,key=119)
            elif blue_event[1]=="pev2_97":
                event=pygame.event.Event(2,key=97)
            elif blue_event[1]=="pev3_97":
                event=pygame.event.Event(3,key=97)
            elif blue_event[1]=="pev2_100":
                event=pygame.event.Event(2,key=100)
            elif blue_event[1]=="pev3_100":
                event=pygame.event.Event(3,key=100)
            else:
                event=pygame.event.Event(3,key=99)
            #print("event")
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    gc.ship_blue.turning_left = True
                    #print("a",state.frames_elapsed)
                elif event.key == pygame.K_d:
                    gc.ship_blue.turning_right = True
                    #print("d")
                elif event.key == pygame.K_w:
                    gc.ship_blue.boosting = True
                    #print("w")
                elif event.key == pygame.K_SPACE:
                    if not state.ship_blue_destroyed:
                        fire_bullet(ai_settings,screen,gc.ship_blue,gc.bullets,state.bgoffset_x,state.bgoffset_y)
                elif event.key == pygame.K_c:
                    if not state.gc.ship_blue_destroyed:
                        fire_charge(ai_settings,screen,gc.ship_blue,gc.charges)
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_a:
                    #print("aup",state.frames_elapsed)
                    gc.ship_blue.turning_left = False
                elif event.key == pygame.K_d:
                    #print("dup",state.frames_elapsed)
                    gc.ship_blue.turning_right = False
                elif event.key == pygame.K_w:
                    #print("wup",state.frames_elapsed)
                    gc.ship_blue.boosting = False



def check_menu_events(state,ai_settings,tc):
    """Respond to keypresses and mouse events."""
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                break
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            elif text_box.active:
                text_box.text += event.unicode
                #print(text_box.text)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            text_box.active=False
            if check_button(state,tc.free_play_button,mouse_x,mouse_y,ai_settings):
                state.game_Active=True
                state.menu_Active=False
                state.free_play=True
                state.time_trial=False
                state.seed_string=text_box.text
                
            elif check_button(state,tc.time_trial_button,mouse_x,mouse_y):
                state.game_Active=True
                state.menu_Active=False
                state.free_play=False
                state.time_trial=True
                state.seed_string=text_box.text

            elif check_button(state,tc.options_button,mouse_x,mouse_y):
                state.options_Active=True
            elif check_button(state,tc.credits_button,mouse_x,mouse_y):
                state.credits_Active=True
                
            elif check_button(state,tc.quit_button,mouse_x,mouse_y):
                state.quit_Active=True
                sys.exit()
            elif text_box.rect.collidepoint(mouse_x,mouse_y):
                text_box.active=True

def update_menu_screen(state, ai_settings, screen, tc):
     #Refresh the background
    screen.fill(ai_settings.bg_menu_color)
    tc.free_play_button.draw_button()
    tc.time_trial_button.draw_button()
    tc.options_button.draw_button()
    tc.credits_button.draw_button()
    tc.quit_button.draw_button()
    tc.text_box.draw_button()
    tc.title.draw_text()
    
    pygame.display.flip()




def check_button(state,button,mouse_x,mouse_y):
    if button.rect.collidepoint(mouse_x,mouse_y):
        print("The button was pushed")
        return True
    
    return False

def fire_bullet(ai_settings,screen,ship,bullets,bgoffset_x,bgoffset_y):
    if len(bullets) <ai_settings.bullets_allowed:
        new_bullet = Bullet(ai_settings, screen, ship,bgoffset_x,bgoffset_y)
        ship.recoil()
        bullets.add(new_bullet)

def fire_charge(ai_settings,screen,ship,charges):
    if len(charges) <ai_settings.bullets_allowed:
        new_charge = Charge(ai_settings, screen, ship)
        #ship.recoil()
        charges.add(new_charge)

def fire_exhaust(ai_settings,screen,ship,exhausts,bgoffset_x,bgoffset_y):
    if len(exhausts) <ai_settings.bullets_allowed:
        new_exhaust = Exhaust(ai_settings, screen, ship,bgoffset_x,bgoffset_y)
        #ship.recoil()
        exhausts.add(new_exhaust)


def update_bullets(bullets,gameRect):
    bullets.update()
    """for bullet in bullets.copy():
        if bullet.x > gameRect.right or bullet.x<gameRect.left or bullet.y>gameRect.bottom or bullet.y<gameRect.top:
            bullets.remove(bullet)"""
    #print(len(bullets))

def update_asteroids(asteroids,gameRect,state):
    asteroids.update(state)
    #print(len(asteroids)," asteroids")

def update_asteroid_pixels(asteroid_pixels,gameRect,state):
    asteroid_pixels.update(state)
    #print(len(asteroids)," asteroids")

def update_comets(comets,gameRect):
    for comet in comets.copy():
        if (comet.x > gameRect.right+30 and comet.x+comet.tail_x > gameRect.right+30) or (comet.x<gameRect.left-30 and comet.x+comet.tail_x <gameRect.left-30) or (comet.y>gameRect.bottom+30 and comet.y+comet.tail_y>gameRect.bottom+30) or (comet.y<gameRect.top-30 and comet.y+comet.tail_y<gameRect.top-30):
                comets.remove(comet)
    comets.update()
    #print(len(comets)," comets")

def update_charges(charges,asteroids,ship,gameRect,ai_settings):
    charges.update()
    for charge in charges:
        detect_charge_asteroid_collision(ai_settings,charge,ship,asteroids)
    for charge in charges.copy():
        if charge.expired:
            charges.remove(charge)
    #print(len(charges)," charges")

def update_exhausts(exhausts,asteroids,ship,gameRect,ai_settings):
    exhausts.update()
    #detect_exhaust_asteroid_collision(ai_settings,ship,exhausts,asteroids)
    for exhaust in exhausts.copy():
        if exhaust.expired:
            exhausts.remove(exhaust)
    #print(len(exhausts)," exhausts")

def update_screen(ai_settings, screen, gc, tc,state):
 
    screen.fill((0,0,0,255))
    for starfield in gc.starfield_pixels1.sprites():
        starfield.draw_starfield(state,ai_settings,screen)
    for starfield in gc.starfield_pixels2.sprites():
        starfield.draw_starfield(state,ai_settings,screen)
    for starfield in gc.starfield_pixels3.sprites():
       starfield.draw_starfield(state,ai_settings,screen)

    for planet in gc.planets.sprites():
        planet.draw_planet(state,ai_settings,screen)

    #for comet in gc.comets.sprites():
    #    comet.draw_comet(gc.gameRect,state.bgoffset_x,state.bgoffset_y)
    for bullet in gc.bullets.sprites():
        bullet.draw_bullet(gc.gameRect,state.bgoffset_x,state.bgoffset_y)

    if state.time_trial or state.story_mode:
        gc.start_line.draw_finish_line(gc.gameRect,state)
        gc.finish_line.draw_finish_line(gc.gameRect,state)
    gc.draw_lap_lines(state,ai_settings)


    for asteroid_pixel in gc.asteroid_pixels.sprites():
        asteroids=asteroid_pixel.asteroids
        for asteroid in asteroids.sprites():
            asteroid.draw_asteroid(gc.gameRect,state.bgoffset_x,state.bgoffset_y,ai_settings)
        pulsars = asteroid_pixel.pulsars
        for pulsar in pulsars.sprites():
            pulsar.draw_pulsar(gc.gameRect,state.bgoffset_x,state.bgoffset_y,state)
            #print("Ast at, ",asteroid.x,",",asteroid.y)

    #for starfield3_pixel in starfield3_pixels

    for exhaust in gc.exhausts.sprites():
        exhaust.draw_exhaust(gc.gameRect,state.bgoffset_x,state.bgoffset_y)
    for charge in gc.charges.sprites():
        charge.draw_charge(gc.gameRect,state.bgoffset_x,state.bgoffset_y)
    gc.ship.blitme(state,ai_settings,screen,gc.gameRect)
    if state.story_mode:
        gc.ship_blue.blitme(state,ai_settings,screen,gc.gameRect)
    #print("sb.x,sb.y: ",ship_blue.centerx,ship_blue.centery)
    #print("s.x,s.y: ",ship.centerx,ship.centery)
    """if state.time_trial:
        timer.draw_timer(state)
        record_timer.draw_timer(state)"""
    
    tc.draw_text(state)

    """tc.timer.draw_timer(state)
    tc.record_timer.draw_timer(state)
    tc.menu_button.draw_button()
    tc.retry_button.draw_button()"""
    #Make the most recently drawn screen visible.
    pygame.display.flip()




def draw_poly(screen,poly,x,y,rot,ai_settings):
    #try: poly.shape[0] < 2:
    #    except: print 'poly object is a large enough array'
    #try: poly.shape[1] != 3:
    #    except: print 'poly object needs to have 3 columns: x, y, and color index'
    
    rotpoly=rot_poly(poly,x,y,rot)
    length=len(poly)
    color_index=0
    color = (255,255,255)
    x1=rotpoly[0,0]
    y1=rotpoly[0,1]
    for i in range (1,length):
        color_index=poly[i,2]
        color=getcolorfromindex(color_index,ai_settings)
        x2=rotpoly[i,0]
        y2=rotpoly[i,1]
        pygame.draw.line(screen, color, [x1,y1], [x2,y2], 3)
        x1=x2
        y1=y2

def rot_poly(poly,x,y,rot,sin_rot=-1,cos_rot=-1):
    length=len(poly)
    poly_done_tmp=poly.copy()
    poly_done=poly_done_tmp.astype(float)
    if sin_rot==-1 or cos_rot==-1:
        rot=-rot
        cos_rot = math.cos(rot)
        sin_rot = math.sin(rot)
    for i in range (0,length):
        poly_done[i,0]=x + cos_rot*poly[i,0] - sin_rot*poly[i,1]
        poly_done[i,1]=y + sin_rot*poly[i,0] + cos_rot*poly[i,1]

    return poly_done



def getcolorfromindex(color_index,ai_settings):
    if ai_settings.color_scheme==2:
        if color_index==1:
            return (255,255,255)
        elif color_index==2:
            return (120,20,20)

        elif color_index==3:
            return (255,60,60)
        elif color_index==4:
            return (140,140,140)
        elif color_index==5:
            return (0,40,255)

    elif ai_settings.color_scheme==3:
        if color_index==1:
            return (255,255,255)
        elif color_index==2:
            return (10,140,240)
        elif color_index==3:
            return (4,40,250)
        elif color_index==4:
            return (180,190,230)
        elif color_index==5:
            return (0,40,255)

    elif ai_settings.color_scheme==4:
        if color_index==1:
            return (255,255,255)
        elif color_index==2:
            return (255,0,0)
        elif color_index==21:
            return (240,0,0)
        elif color_index==22:
            return (225,0,0)
        elif color_index==23:
            return (210,0,0)
        elif color_index==24:
            return (195,0,0)
        elif color_index==25:
            return (180,0,0)
        elif color_index==26:
            return (165,0,0)
        elif color_index==27:
            return (150,0,0)
        elif color_index==28:
            return (135,0,0)
        elif color_index==29:
            return (120,0,0)

        elif color_index==3:
            return (0,255,0)
        elif color_index==31:
            return (0,240,0)
        elif color_index==32:
            return (0,225,0)
        elif color_index==33:
            return (0,210,0)
        elif color_index==34:
            return (0,195,0)
        elif color_index==35:
            return (0,180,0)
        elif color_index==36:
            return (0,165,0)
        elif color_index==37:
            return (0,150,0)
        elif color_index==38:
            return (0,135,0)
        elif color_index==39:
            return (0,120,0)

        elif color_index==4:
            return (0,0,255)
        elif color_index==41:
            return (0,0,240)
        elif color_index==42:
            return (0,0,225)
        elif color_index==43:
            return (0,0,210)
        elif color_index==44:
            return (0,0,195)
        elif color_index==45:
            return (0,0,180)
        elif color_index==46:
            return (0,0,165)
        elif color_index==47:
            return (0,0,150)
        elif color_index==48:
            return (0,0,135)
        elif color_index==49:
            return (0,0,120)
        elif color_index==5:
            return (0,40,255)
    
    else:
        if color_index==1:
            return (255,255,255)
        elif color_index==2:
            return (255,255,255)
    
        elif color_index==3:
            return (130,130,130)
        elif color_index==4:
            return (190,190,190)
        elif color_index==5:
            return (0,40,255)

    return (255,255,255)

def detect_charge_asteroid_collision(ai_settings,charge,ship,asteroids,bgoffset_x,bgoffset_y):
    collision_vector_x = 0
    collision_vector_y = 0
    collision_vector_x_norm = 0
    collision_vector_y_norm = 0
    collision_vector_length = 0
    collision_vector_length_sq = 0
    for asteroid in asteroids:
        if (charge.x < asteroid.rect.right and charge.x > asteroid.rect.left and charge.y < asteroid.rect.bottom and charge.y > asteroid.rect.top):
            #print("Asteroid Collision")
            charge.expired=True
            collision_vector_x = ship.centerx-asteroid.x
            collision_vector_y = ship.centery-asteroid.y
            #collision_vector=(ship.centerx-asteroid.x,ship.centery-asteroid.y)
            collision_vector_length_sq=collision_vector_x*collision_vector_x+collision_vector_y+collision_vector_y
            if  collision_vector_length_sq <=0.0001:
                collision_vector_length_sq =0.0001
            collision_vector_length=math.sqrt(collision_vector_length_sq)
            if collision_vector_length<.1:
                collision_vector_length=.1
            collision_vector_x_norm=collision_vector_x/collision_vector_length
            
            #in case there was a math error, limit norm values to -1,1
            if collision_vector_x_norm>1:
                collision_vector_x_norm=1
            elif collision_vector_x_norm<-1:
                collision_vector_x_norm=-1
                
            if collision_vector_y_norm>1:
                collision_vector_y_norm=1
            elif collision_vector_y_norm<-1:
                collision_vector_y_norm=-1

            collision_vector_y_norm=collision_vector_y/collision_vector_length
            
            #print(collision_vector_x_norm," ",collision_vector_y_norm, " ", collision_vector_length, " ", collision_vector_x, " ", collision_vector_y)
            #collision_vector_len=math.sqrt(collision_vector_x*collision_vector_x+collision_vector_y+collision_vector_y)
            if (ship.x_vel > -5 and ship.x_vel < 5):
                ship.x_vel -= collision_vector_x_norm/10.0
            if (ship.y_vel > -5 and ship.y_vel < 5):
                ship.y_vel -= collision_vector_y_norm/10.0
            break



def polys_collide(poly1,poly2):
    length1=len(poly1)-1
    length2=len(poly2)-1
    for i in range (0,length1):
        for j in range (0,length2):
            #print("i,j: ",i," ",j)
            if do_two_line_segments_intersect(poly1[i,0],poly1[i,1],poly1[i+1,0],poly1[i+1,1],poly2[j,0],poly2[j,1],poly2[j+1,0],poly2[j+1,1]):
                side1=i
                side2=j
                
                return (True,side1,side2)
    return (False,0,0)

def poly_point_collide(poly,point):
    crosses=0
    #point2=point.copy
    #point2[0]+=200
    length1=len(poly)-1
    #print("point, ", point)
    #print("poly, ", poly)
    for i in range (0,length1):
        if do_two_line_segments_intersect(point[0],point[1],point[0]+200,point[1],poly[i,0],poly[i,1],poly[i+1,0],poly[i+1,1]):
            crosses+=1
            print("cross")
    if (crosses%2)==1:
        return True
    return False

def do_two_line_segments_intersect(a1,b1,a2,b2,c1,d1,c2,d2):
    if (a1>c1 and a2>c1 and a1>c1 and a2>c2):
        return False
    if (a1<c1 and a2<c1 and a1<c1 and a2<c2):
        return False
    if (b1>d1 and b2>d1 and b1>d1 and b2>d2):
        return False
    if (b1<d1 and b2<d1 and b1<d1 and b2<d2):
        return False
    n1=(b1-b2,a2-a1)
    h1=(c1-a1,d1-b1)
    h2=(c2-a1,d2-b1)
    n1doth1=n1[0]*h1[0]+n1[1]*h1[1]
    n1doth2=n1[0]*h2[0]+n1[1]*h2[1]
    
    if(n1doth1>0 and n1doth2<0) or (n1doth1<0 and n1doth2>0):
        
        n2=(d1-d2,c2-c1)
        t1=(a2-c1,b2-d1)
        t2=(a1-c1,b1-d1)
        n2dott1=n2[0]*t1[0]+n2[1]*t1[1]
        n2dott2=n2[0]*t2[0]+n2[1]*t2[1]
        if(n2dott1>0 and n2dott2<0) or (n2dott1<0 and n2dott2>0):
            """print("ndoth1,2: ",n1doth1,n1doth2)
            print("a1,b1,a2,b2,c1,d1,c2,d2",a1,b1,a2,b2,c1,d1,c2,d2)
            print("n, h1, h2,",n1, h1, h2)
            print("ndott1,2: ",n2dott1,n2dott2)
            print("n, h1, h2,",n2, t1, t2)"""
            return True
    return False

def detect_exhaust_asteroid_collision(ai_settings,ship,exhausts,asteroids):
    for exhaust in exhausts:
        if exhaust.can_hit:
            for asteroid in asteroids:
                #if not asteroid.rect.collidepoint(exhaust.x,exhaust.y):
                #    return False
                bob=0
                #if (ship.offset_centerx < asteroid.rect.right and ship.offset_centerx > asteroid.rect.left and ship.offset_centery < asteroid.rect.bottom and ship.offset_centery > asteroid.rect.top):
                if True:
                
                    if poly_point_collide(rot_poly(asteroid.poly,asteroid.x,asteroid.y,asteroid.dir_facing),(exhaust.x,exhaust.y)):
                        print("exhaust hit")
                        exhaust.can_hit=False
                        exhaust.x_vel=-exhaust.x_vel
                        exhaust.y_vel=-exhaust.y_vel
    return False

def detect_ship_asteroid_pixels_collision(ai_settings,ship,asteroid_pixels,bgoffset_x,bgoffset_y):
    
    for asteroid_pixel in asteroid_pixels:
        asteroids=asteroid_pixel.asteroids
        detect_ship_asteroid_collision(ai_settings,ship,asteroids,bgoffset_x,bgoffset_y)

def detect_ship_asteroid_collision(ai_settings,ship,asteroids,bgoffset_x,bgoffset_y):
    #collision_vector=(0,0)
    ship_offset_centerx = ship.centerx-bgoffset_x
    ship_offset_centery = ship.centery-bgoffset_y
    shippolytmp=ship.poly.copy()
    shippoly=shippolytmp.astype(float)
    shippoly=rot_poly(shippoly,ship_offset_centerx,ship_offset_centery,ship.dir_facing)
    
    #print("vel ",ship.x_vel,ship.y_vel)
    #print(ship.poly)
    #print(shippoly)
    collision_vector_x = 0
    collision_vector_y = 0
    collision_vector_x_norm = 0
    collision_vector_y_norm = 0
    collision_vector_length = 0
    collision_vector_length_sq = 0
    collision_params=(False,0,0)
    ast_face_angle=0
    (i,j)=(0,0)
    a=0
    x1=0
    x2=0
    y1=0
    y2=0
    collision_severity=0
    angle_off=0
    
    for asteroid in asteroids:
            
        if (ship_offset_centerx < asteroid.rect.right and ship_offset_centerx > asteroid.rect.left and ship_offset_centery < asteroid.rect.bottom and ship_offset_centery > asteroid.rect.top):
        
            asteroid_poly=rot_poly(asteroid.poly,asteroid.x,
            asteroid.y,asteroid.dir_facing)
            
            collision_params=polys_collide(shippoly,asteroid_poly)
            if collision_params[0]:
                #print("Asteroid Collision at sides i,j: ",collision_params[1]," ",collision_params[2])
                a=collision_params[2]
                x1=asteroid_poly[a,0]
                x2=asteroid_poly[a+1,0]
                y1=asteroid_poly[a,1]
                y2=asteroid_poly[a+1,1]
                #print("x1,x2,y1,y2: ",x1,x2,y1,y2)
                #ast_face_angle=face_angle(asteroid_poly,collision_params[2])
                
                #plus or minus asteroid.dir_facing???
                ast_face_angle=math.atan2((y2-y1),(x2-x1))
                ship2ast_angle=math.atan2((ship.centery-asteroid.y),(ship.centerx-asteroid.x))
                if math.fabs(ship2ast_angle-ast_face_angle)%(3.1415926535)>3.1415926535/2:
                    ast_face_angle+=3.1415926535
                
                
                angle_off=(ship.heading_angle-ast_face_angle+3.1415926535/2)%(3.1415926535)
                #print("angle_off: ",angle_off)
                collision_severity=angle_off*angle_off*ship.heading_speed*ship.heading_speed
                if collision_severity>25:
                    ship.damage+=collision_severity-25
                collision_severity=max(0,collision_severity)
                print("ship.damage: ",ship.damage)
                #print("collision_severity",collision_severity)
                
                #print("s,a poly: ",shippoly,asteroid_poly)
                
                #there is an ambiguity in which direction normal_angle points, up to a 180 degree
                #rotation.
                
                normal_angle=(ast_face_angle-3.1415926535/2)
                speed_change=collision_severity/100
                dx=speed_change*math.sin(normal_angle)
                dy=speed_change*math.cos(normal_angle)
                dx_norm=math.sin(normal_angle)
                dy_norm=math.cos(normal_angle)
                
                
                """ship.centerx += dx_norm * 5
                ship.centery += dy_norm * 5
                ship_heading_off_normal = ship.heading_angle - normal_angle
                ship_new_heading = ship.heading_angle -2 * ship_heading_off_normal
                ship.x_vel = ship.heading_speed * math.sin(ship_new_heading) * 0.78
                ship.y_vel = ship.heading_speed * math.cos(ship_new_heading) * 0.78"""
                #print("shippoly,asteroid_poly",shippoly,asteroid_poly)
                #print("ast_face_angle,ship.heading_angle,normal_angle,dx,dy,dx_norm,dy_norm,ship_heading_off_normal:  ",ast_face_angle,ship.heading_angle,normal_angle,dx,dy,dx_norm,dy_norm,ship_heading_off_normal)
                collision_vector_x = ship_offset_centerx-asteroid.x
                collision_vector_y = ship_offset_centery-asteroid.y
                #collision_vector=(ship.centerx-asteroid.x,ship.centery-asteroid.y)
                collision_vector_length_sq=collision_vector_x*collision_vector_x+collision_vector_y+collision_vector_y
                if  collision_vector_length_sq <=0.0001:
                    collision_vector_length_sq =0.0001
                collision_vector_length=math.sqrt(collision_vector_length_sq)
                if collision_vector_length<.1:
                    collision_vector_length=.1
                collision_vector_x_norm=collision_vector_x/collision_vector_length
                collision_vector_y_norm=collision_vector_y/collision_vector_length
            
                #in case there was a math error, limit norm values to -1,1
                if collision_vector_x_norm>1:
                    collision_vector_x_norm=1
                elif collision_vector_x_norm<-1:
                    collision_vector_x_norm=-1
            
                if collision_vector_y_norm>1:
                    collision_vector_y_norm=1
                elif collision_vector_y_norm<-1:
                    collision_vector_y_norm=-1
                
                #collision_vector_len=math.sqrt(collision_vector_x*collision_vector_x+collision_vector_y+collision_vector_y)
                if (ship.x_vel > -50 and ship.x_vel < 50):
                    ship.x_vel = ship.x_vel
                    ship.x_vel += collision_vector_x_norm
                if (ship.y_vel > -50 and ship.y_vel < 50):
                    ship.y_vel = ship.y_vel
                    ship.y_vel += collision_vector_y_norm
                ship.centerx +=collision_vector_x_norm*11
                ship.centery +=collision_vector_y_norm*11
                ship.update_heading()
                #ship.delaythrustx += collision_vector_x_norm
                #ship.delaythrusty += collision_vector_y_norm

def set_scroll_left(ai_settings):
    ai_settings.scroll_left = 800
    ai_settings.scroll_right = 300
    ai_settings.scroll_top = 300
    ai_settings.scroll_bottom = 300

def set_scroll_right(ai_settings):
    ai_settings.scroll_left = 300
    ai_settings.scroll_right = 800
    ai_settings.scroll_top = 300
    ai_settings.scroll_bottom = 300

def set_scroll_top(ai_settings):
    ai_settings.scroll_left = 300
    ai_settings.scroll_right = 300
    ai_settings.scroll_top = 600
    ai_settings.scroll_bottom = 300

def set_scroll_bottom(ai_settings):
    ai_settings.scroll_left = 300
    ai_settings.scroll_right = 300
    ai_settings.scroll_top = 300
    ai_settings.scroll_bottom = 600

def scroll_border(ai_settings,ship,ship_blue,gameRect,bgrect_big,bgoffset_x,bgoffset_y):
    
    l = 0
    r = 0
    t = 0
    b = 0
    
    while (ship.centerx<gameRect.left+ai_settings.scroll_left) and l<=ai_settings.lsmi:
        ship.centerx+=1
        #ship_blue.centerx-=1
        ship_blue.centerx+=1
        bgoffset_x=bgoffset_x+1
        l+=1
        if l >ai_settings.lsmi:
            #ai_settings.lsmi*=2
            ai_settings.lsmi+=2
            break
    while (ship.centerx>gameRect.right-ai_settings.scroll_right)and r<=ai_settings.rsmi:
        ship.centerx-=1
        #ship_blue.centerx+=1
        ship_blue.centerx-=1
        bgoffset_x=bgoffset_x-1
        r+=1
        if r >ai_settings.rsmi:
            #ai_settings.rsmi*=2
            ai_settings.rsmi+=2
            break
    while (ship.centery<gameRect.top+ai_settings.scroll_top):
        ship.centery+=1
        #ship_blue.centery-=1
        ship_blue.centery+=1
        bgoffset_y=bgoffset_y+1
        t+=1
        if t >ai_settings.tsmi:
            #ai_settings.tsmi*=2
            ai_settings.tsmi+=2
            break
    while (ship.centery>gameRect.bottom-ai_settings.scroll_bottom):
        ship.centery-=1
        #ship_blue.centery+=1
        ship_blue.centery-=1
        bgoffset_y=bgoffset_y-1
        b+=1
        if b >ai_settings.bsmi:
            #ai_settings.bsmi*=2
            ai_settings.bsmi+=2
            break
    #print("r,l,t,b,rsmi,lsmi,tsmi,bsmi: ", r,l,t,b,ai_settings.rsmi,ai_settings.lsmi,ai_settings.tsmi,ai_settings.bsmi)

    if ai_settings.lsmi > 1 and l < ai_settings.lsmi/2:
        ai_settings.lsmi/=2
    if ai_settings.rsmi > 1 and r < ai_settings.rsmi/2:
        ai_settings.rsmi/=2
    if ai_settings.tsmi > 1 and t < ai_settings.tsmi/2:
        ai_settings.tsmi/=2
    if ai_settings.bsmi > 1 and b < ai_settings.bsmi/2:
        ai_settings.bsmi/=2

    return (bgoffset_x,bgoffset_y)

def update_lap_line_text(ai_settings,state,gc,tc,cl,cll):
    #gc - game config
    #tc - text config
    #cl - current lap
    #cll - current lap line
    if cl == 1:
        state.lap1_list[cll]='b'
        msg=''.join(state.lap1_list)
        tc.lap1_info_text.prep_msg(msg)
    elif cl == 2:
        state.lap2_list[cll]='b'
        msg=''.join(state.lap2_list)
        tc.lap2_info_text.prep_msg(msg)
    elif cl == 3:
        state.lap3_list[cll]='b'
        msg=''.join(state.lap3_list)
        tc.lap3_info_text.prep_msg(msg)
    elif cl == 4:
        state.lap4_list[cll]='b'
        msg=''.join(state.lap4_list)
        tc.lap4_info_text.prep_msg(msg)

    return

def check_lap_crossings(ai_settings,state,gc,tc):

    cll=state.current_lap_line
    cl=state.current_lap
    update=False
    if(cll==0):
        if check_line_cross(gc.ship,gc.lap_line0,state):
            state.current_lap_line=cll+1
            update=True
            print("line 0 was crossed")
    
    elif(cll==1):
        if check_line_cross(gc.ship,gc.lap_line1,state):
            state.current_lap_line=cll+1
            update=True

            print("line 1 was crossed")

    elif(cll==2):
        if check_line_cross(gc.ship,gc.lap_line2,state):
            state.current_lap_line=cll+1
            update=True

            print("line 2 was crossed")

    elif(cll==3):
        if check_line_cross(gc.ship,gc.lap_line3,state):
            state.current_lap_line=cll+1
            update=True
            print("line 3 was crossed")

    elif(cll==4):
        if check_line_cross(gc.ship,gc.lap_line4,state):
            state.current_lap_line=cll+1
            update=True
            print("line 4 was crossed")
    elif(cll==5):
        if check_line_cross(gc.ship,gc.lap_line5,state):
            state.current_lap_line=cll+1
            update=True
            print("line 5 was crossed")
    elif(cll==6):
        if check_line_cross(gc.ship,gc.lap_line6,state):
            state.current_lap_line=cll+1
            update=True
            print("line 6 was crossed")
    elif(cll==7):
        if check_line_cross(gc.ship,gc.lap_line7,state):
            state.current_lap_line=cll+1
            update=True
            print("line 7 was crossed")

    elif(cll==8):
        if check_line_cross(gc.ship,gc.lap_line8,state):
            state.current_lap_line=cll+1
            update=True
            print("line 8 was crossed")

    elif(cll==9):
        if check_line_cross(gc.ship,gc.lap_line9,state):
            state.current_lap_line=cll+1
            update=True
            print("line 9 was crossed")

    elif(cll==10):
        if check_line_cross(gc.ship,gc.lap_line10,state):
            state.current_lap_line=cll+1
            update=True
            print("line 10 was crossed")

    if state.current_lap_line>state.max_lap_line:
        state.current_lap+=1
        if state.current_lap>state.max_lap:
            state.crossed_finish=True
            state.crossed_finish_frame=state.frames_elapsed
            state.current_lap=0
            state.current_lap_line=0
    #print("update,cll,cl=",update,cll,cl)
    if update:
        update_lap_line_text(ai_settings,state,gc,tc,cl,cll)
        return True

    return False

def check_line_cross(ship,lap_line,state):
    ship_offset_centerx = ship.centerx-state.bgoffset_x
    ship_offset_centery = ship.centery-state.bgoffset_y
    shippolytmp=ship.poly.copy()
    shippoly=shippolytmp.astype(float)
    shippoly=rot_poly(shippoly,ship_offset_centerx,ship_offset_centery,ship.dir_facing)
    lsx=lap_line.line_start_shifted_x
    lsy=lap_line.line_start_shifted_y
    lex=lap_line.line_end_shifted_x
    ley=lap_line.line_end_shifted_y

    linepoly=numpy.array([
        [lap_line.line_start_x,lap_line.line_start_y,1],
        [lap_line.line_end_x,lap_line.line_end_y,1]])
    #linepoly=numpy.array([
    #    [lsx,lsy,1],
    #    [lex,ley,1]])
    
    #if (ship_offset_centerx < asteroid.rect.right and ship_offset_centerx > asteroid.rect.left and ship_offset_centery < asteroid.rect.bottom and ship_offset_centery > asteroid.rect.top):
    #print("shippoly, linepoly", shippoly, linepoly)
    collision_params=polys_collide(shippoly,linepoly)
    if collision_params[0]:
        print ("collide")
        return True
        
    return False

def l2g_coords(point):
    return point
def g2l_coords(point):
    return point

def save_score(score,initials):
    d  = shelve.open('score.txt')
    d['score'] = score

def resource_path(relative):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative)
    return os.path.join(relative)

def shrink_poly(poly,factor,ai_settings):
    if factor==0:
        #We don't want to divide by zero.
        return poly
    top=0
    bottom=0
    left=0
    right=0
    poly_copy=poly.copy()
    
    length=len(poly_copy)
    
    for i in range (0,length):
        top=min(poly_copy[i,1],top)
        bottom=max(poly_copy[i,1],bottom)
        left=min(poly_copy[i,0],left)
        right=max(poly_copy[i,0],right)
    width=right-left
    height=bottom-top
    desired_width=100
    desired_height=80
    l_factor=width/desired_width
    h_factor=height/desired_height
    fact=max(l_factor,h_factor)
    print("top,bottom,left,right=",top,bottom,left,right)
    print("width,height=",width,height)
    for i in range (0,length):
        poly_copy[i,0] += -left
        poly_copy[i,1] += -top
        poly_copy[i,0] /= fact
        poly_copy[i,1] /= fact
        poly_copy[i,0] += ai_settings.screen_width-width/fact
        poly_copy[i,1] += ai_settings.screen_height-height/fact
    return poly_copy


def detect_ship_rails_collision(ai_settings,ship,rails,bgoffset_x,bgoffset_y):
    #collision_vector=(0,0)
    ship_offset_centerx = ship.centerx-bgoffset_x
    ship_offset_centery = ship.centery-bgoffset_y
    shippolytmp=ship.poly.copy()
    shippoly=shippolytmp.astype(float)
    shippoly=rot_poly(shippoly,ship_offset_centerx,ship_offset_centery,ship.dir_facing)
    
    #print("vel ",ship.x_vel,ship.y_vel)
    #print(ship.poly)
    #print(shippoly)
    collision_vector_x = 0
    collision_vector_y = 0
    collision_vector_x_norm = 0
    collision_vector_y_norm = 0
    collision_vector_length = 0
    collision_vector_length_sq = 0
    collision_params=(False,0,0)
    rail_face_angle=0
    (i,j)=(0,0)
    a=0
    x1=0
    x2=0
    y1=0
    y2=0
    collision_severity=0
    angle_off=0
    
            
    #if (ship_offset_centerx < rails.rect.right and ship_offset_centerx > rails.rect.left and ship_offset_centery < rails.rect.bottom and ship_offset_centery > rails.rect.top):
    """if True:
        
        collision_params=polys_collide(shippoly,rails.rail1poly)
        if collision_params[0]:
            railhit=1
        else:
            collision_params=polys_collide(shippoly,rails.rail2poly)
            railhit=2
        if collision_params[0]:
            #print("Asteroid Collision at sides i,j: ",collision_params[1]," ",collision_params[2])
            a=collision_params[2]
            if railhit==1:
                x1=rails.rail1poly[a,0]
                x2=rails.rail1poly[a+1,0]
                y1=rails.rail1poly[a,1]
                y2=rails.rail1poly[a+1,1]
            else:
                x1=rails.rail2poly[a,0]
                x2=rails.rail2poly[a+1,0]
                y1=rails.rail2poly[a,1]
                y2=rails.rail2poly[a+1,1]

            ast_face_angle=math.atan2((y2-y1),(x2-x1))"""


            
