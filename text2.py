import pygame.font
import game_functions as gf
import os

class Text2():
    def __init__(self, ai_settings, screen, msg,height_down=0,align_mode=0):
        """Initialize button attributes."""
        self.screen = screen
        self.screen_rect = screen.get_rect()
    
        #Set the dimensions and properties of the button.
        self.width, self.height = 200, 50
        if ai_settings.color_scheme==2:
            self.button_color = (200, 0, 0)
            self.text_color = (40,0,0)
        else:
            self.button_color = (0, 170, 0)
            self.text_color = (255,255,170)

        filename = "OpenSans-Regular.ttf"
        myfontfile = gf.resource_path(os.path.join("data", filename))
        self.font = pygame.font.Font(myfontfile, 36)
        
        #Build the button's rect object and center it.
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        if align_mode==0:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.left)
        elif align_mode==1:
            (self.rect.top,self.rect.centerx) = (self.screen_rect.top+height_down,self.screen_rect.centerx)
        elif align_mode==2:
            (self.rect.top,self.rect.right) = (self.screen_rect.top+height_down,self.screen_rect.right)
        
        else:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.centerx)
        #The button message needs to be prepped only once.
        self.prep_msg(msg)

    def prep_msg(self, msg):
        """Turn msg into a rendered image and center text on the button."""
        self.msg_image = self.font.render(msg, True, self.text_color)
        self.msg_image_rect = self.msg_image.get_rect()
        
        width = max(200, self.msg_image.get_width()+10)
        self.rect.width = width
        self.msg_image_rect.center = self.rect.center

    def draw_text(self):
        #Draw blank button and then draw message
        #self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)
