import pygame.font
import state
import game_functions as gf
import os

class Timer():
    def __init__(self, ai_settings, screen,height_down=0,align_mode=1,counting=True,msg="00:00:00"):
        """Initialize button attributes."""
        self.msg=msg
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.counting=counting
        #Set the dimensions and properties of the button.
        self.width, self.height = 200, 50
        if ai_settings.color_scheme==2:
            self.button_color = (100, 0, 0)
            self.text_color = (240,0,0)
        else:
            self.button_color = (0, 170, 0)
            self.text_color = (255,255,170)
        
        filename = "OpenSans-Regular.ttf"
        myfontfile = gf.resource_path(os.path.join("data", filename))
        self.font = pygame.font.Font(myfontfile, 36)
        
               #Build the button's rect object and center it.
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        if align_mode==0:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.left)
        elif align_mode==1:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.centerx)
        else:
            (self.rect.top,self.rect.left) = (self.screen_rect.top+height_down,self.screen_rect.centerx)
        self.prep_msg(self.msg)

    def stop_watch(self,value):
        """From seconds to Days;Hours:Minutes;Seconds"""
        #valueD = (((value/365)/24)/60) "525,600 minutes, 525,600 moments so dear"
        valueD=value/525600
        Days = int(valueD)
            
        valueH=(valueD-Days)*365
        Hours = int(valueH)

        valueM = (valueH - Hours)*24
        Minutes=int(valueM)
            
        valueS = (valueM - Minutes)*60
        Seconds = int(valueS)
        
        valueCS = (valueS - Seconds)*100
        CentiSeconds = int(valueCS)
            
        #print (Days,";",Hours,":",Minutes,";",Seconds)
        return str(Minutes).zfill(2) + ":" + str(Seconds).zfill(2) + ":" +str(CentiSeconds).zfill(2)
            
    def prep_msg(self, msg):
        """Turn msg into a rendered image and center text on the button."""
        self.msg_image = self.font.render(msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_timer(self,state):
        #Draw blank button and then draw message
        if self.counting and state.crossed_start and not state.crossed_finish:
            self.msg=self.stop_watch((state.frames_elapsed-state.frame_start)/1000*60)
            self.prep_msg(self.msg)
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)

    def update_time(self,msg):
        self.prep_msg(msg)

