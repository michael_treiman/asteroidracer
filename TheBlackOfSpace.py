import sys
import os
import pygame
import time
from pygame.sprite import Group
import game_functions as gf
from button2 import Button2
from state import State
from settings import Settings
from ship import Ship
from asteroid import Asteroid
from asteroid_pixel import AsteroidPixel
from starfield import Starfield
from comet import Comet
from planet import Planet
from pulsar import Pulsar
from timer import Timer
from finish_line import Finish_line
from finish_line2 import Finish_line2
from text_box import Text_box
from text2 import Text2
from game_config import Game_config
from text_config import Text_config




def run_game():
    
    ai_settings = Settings()
    state = State()
    pygame.init()
    pygame.mixer.pre_init(44100,16,2,4096)
    #sound = pygame.mixer.Sound("disco_beam.wav")
    pygame.mixer.music.set_volume(.5)
    #pygame.mixer.music.load("pneumatic_drill.ogg")
    music_path = gf.resource_path(os.path.join("data", "dont_stop_the_music.ogg"))

    pygame.mixer.music.load(music_path)
   
    #pygame.mixer.Sound.play(sound)
    clock = pygame.time.Clock()

    blorgus=0
    
    #initialize game and create a screen object.
    #ai_settings = Settings()
    #state = State()
    print("hello world")
    
    if ai_settings.play_music:
        pygame.mixer.music.play(-1)
    
    screen = pygame.display.set_mode((ai_settings.screen_width,ai_settings.screen_height))
    pygame.display.set_caption("Asteroid Racer")
    tc=Text_config(ai_settings, state, screen)
    state.window_active=True

    #Start the main loop for the game
    while state.window_active:
        
            #Watch for keyboard and mouse events
            
        
            
            if state.win_Active:
                blorgus=0

            else:
                if not state.gc_set:
                    gc=Game_config(ai_settings, state, screen)
                    state.gc_set = True
                    state.bgoffset_y=ai_settings.init_bgoffset_y
                
                gc.scroll_grids(state,ai_settings,screen)
                gf.check_events(state,ai_settings, screen,tc,gc)
                (state.bgoffset_x,state.bgoffset_y)=gf.scroll_border(ai_settings,gc.ship,gc.ship_blue,gc.gameRect,
                gc.gameRect,state.bgoffset_x,state.bgoffset_y)

                if gc.ship.boosting and not state.ship_destroyed:
                    gf.fire_exhaust(ai_settings,screen,gc.ship,gc.exhausts,state.bgoffset_x,state.bgoffset_y)

                if gc.ship_blue.boosting and not state.ship_blue_destroyed:
                    gf.fire_exhaust(ai_settings,screen,gc.ship_blue,gc.exhausts,state.bgoffset_x,state.bgoffset_y)

                gc.ship.update(state,ai_settings,screen)
                gc.ship_blue.update(state,ai_settings,screen)
                gf.update_asteroid_pixels(gc.asteroid_pixels,gc.bgrect_big,state)
                gf.update_bullets(gc.bullets,gc.bgrect_big)
                gf.update_comets(gc.comets,gc.bgrect_big)
                gf.update_charges(gc.charges,gc.asteroids,gc.ship,gc.gameRect,ai_settings)
                gf.update_exhausts(gc.exhausts,gc.asteroids,gc.ship,gc.gameRect,ai_settings)

                if not state.ship_destroyed and gc.ship.col_timer<=0:
                    gf.detect_ship_asteroid_pixels_collision(ai_settings,gc.ship,
                    gc.asteroid_pixels,state.bgoffset_x,state.bgoffset_y)
                    gf.detect_ship_rails_collision(ai_settings,gc.ship,
                    gc.asteroid_pixels,state.bgoffset_x,state.bgoffset_y)
                    
                    gc.ship.col_timer=ai_settings.ship_col_frames
                
                if not state.ship_blue_destroyed and gc.ship_blue.col_timer<=0:
                    gf.detect_ship_asteroid_collision(ai_settings,gc.ship_blue,
                    gc.asteroids,state.bgoffset_x,state.bgoffset_y)
                    gc.ship_blue.col_timer=ai_settings.ship_col_frames

                state.frames_elapsed+=1

                gf.check_lap_crossings(ai_settings,state,gc,tc)
                gf.update_screen(ai_settings, screen, gc, tc,state)
                
            #print("frames elapsed ,", state.frames_elapsed)
            clock.tick(60)

    pygame.display.quit()
    state.gc_set=False



            
#run_game()
