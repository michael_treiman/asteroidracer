import pygame
import math
from pygame.sprite import Sprite
from pygame.sprite import Group
from starfield import Starfield
import game_functions
import numpy
import random
from state import State
import game_functions as gf


class StarfieldPixel(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self,i,j,count,layer,typevar,ai_settings,screen):
        """Create a bullet object at the ship's current position."""
        super(StarfieldPixel, self).__init__()
        self.screen = screen
        random.seed(typevar)

        self.i=i
        self.j=j
        extra_bit=50
        self.width=screen.get_width()
        self.height=screen.get_height()
        
        self.starfield = Starfield(ai_settings, screen, layer,extra_bit)
    

    def update(self,state):
        """Placeholder method."""

            



