import pygame
import math
from pygame.sprite import Sprite

class Bullet(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen, ship,bgoffset_x,bgoffset_y):
        """Create a bullet object at the ship's current position."""
        super(Bullet, self).__init__()
        self.screen = screen

        # Create a bullet rect at (0,0) and then set the correct position.
        self.rect = pygame.Rect(0,0,ai_settings.bullet_width,ai_settings.bullet_height)
        self.rect.centerx = ship.centerx+int(ship.x_nib)-bgoffset_x
        self.rect.centery = ship.centery+int(ship.y_nib)-bgoffset_y

        #Store the bullet's position as a decimal value
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        self.color = ai_settings.bullet_color
        self.speed_factor = ai_settings.bullet_speed_factor
    
        self.dir_moving = 0
        self.speed = 0
        self.mass_base=100
        self.screen_rect = screen.get_rect()
    
        self.x_vel=ship.x_vel+math.sin(ship.dir_facing)*6
        self.y_vel=ship.y_vel+math.cos(ship.dir_facing)*6


    def update(self):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        self.x += self.x_vel
        self.y += self.y_vel
        #Update the rect position.
        self.rect.x = self.x
        self.rect.y = self.y
        
    def draw_bullet(self,gameRect,bgoffset_x,bgoffset_y):
        """Draw the bullet to the scree."""
        pygame.draw.rect(self.screen, self.color, self.rect.move(bgoffset_x,bgoffset_y))
