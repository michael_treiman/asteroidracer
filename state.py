import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class State():
    """A class to manage bullets fired from the ship."""

    def __init__(self):
        """Create a bullet object at the ship's current position."""
        super(State, self).__init__()
        self.frames_elapsed=0
        self.frame_start = 0
        self.bgoffset_x = 0
        self.bgoffset_y = 0
        self.menu_Active=False
        self.game_Active=True
        self.options_Active=False
        self.credits_Active=False
        self.review_screen_Active=False
        self.quit_Active=False
        self.win_Active=False

        self.paused=False

        self.free_play=True
        self.time_trial=False
        self.story_mode=False
        self.crossed_finish = False
        self.crossed_finish_frame = 0
        self.crossed_start = False
        self.record_stored = False
        self.record_score = 0
        self.record_string="00:00:00"
        self.ship_destroyed = False
        self.ship_blue_destroyed = False
        self.seed_string = ""
        self.gc_set = False
        self.opponent_present=True
        self.opponent_id=0
        self.blue_win=False
        self.blue_crossed_start=False
        
        self.win=False
        self.lose=False
        

        self.blue_crossed_finish=False
        
        self.window_active=True

        self.current_lap_line=0

        self.current_lap = 1
        
        self.thrust_frozen=True
        self.max_lap_line=10
        self.max_lap=1
        
        """
        self.lap1_list = list("○○○○○○")
        self.lap2_list = list("○○○●●●")
        self.lap3_list = list("○○○○○○")
        self.lap4_list = list("○○○○○○")
        self.lap5_list = list("○○○○○○")
        self.lap6_list = list("○○○○○○")"""

        self.lap1_list = list("aaaaaaaaaaa")
        self.lap2_list = list("aaaaaaaaaaa")
        self.lap3_list = list("aaaaaaaaaaa")
        self.lap4_list = list("aaaaaaaaaaa")
        self.lap5_list = list("aaaaaaaaaaa")
        self.lap6_list = list("aaaaaaaaaaa")

