import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Planet(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen,planet_x=600,planet_y=300):
        """Create a bullet object at the ship's current position."""
        super(Planet, self).__init__()
        self.screen = screen
        self.radius=500
        self.width=2*self.radius
        self.height=2*self.radius
        
        self.offsetfactor=5
        
        self.x=planet_x
        self.y=planet_y
        
        self.planet_screen = pygame.Surface((self.width,self.height),pygame.SRCALPHA)
        self.planet_screen.convert_alpha()
        self.planet_screen.fill((0,0,0,0))
        self.screen_rect = self.planet_screen.get_rect()
    
        c1=[250,120,40]
        c2=[150,80,25]
        #c1=[40,120,250]
        #c2=[25,80,150]
        
        pygame.draw.circle(self.planet_screen,c2,[self.radius, self.radius],self.radius)
        pygame.draw.ellipse(self.planet_screen, c1, self.screen_rect.inflate(-200,1))
        pygame.draw.arc(self.planet_screen, c1, self.screen_rect,3.1415926/2,3.1415926*3/2,0)

    
        #mask = pygame.Surface((self.width,self.height), pygame.SRCALPHA)
        #masked = background.copy()
        #masked.blit(mask, (0, 0), None, pygame.BLEND_RGBA_MULT)
        #display.blit(masked, (0, 0))
        
    def draw_planet(self,state,ai_settings,screen):
        """Draw the planet to the screen."""
        
        bgoffset_x=state.bgoffset_x
        bgoffset_y=state.bgoffset_y
        
        bgoffset_x2=int(bgoffset_x/self.offsetfactor)
        bgoffset_y2=int(bgoffset_y/self.offsetfactor)
        
        self.int_x=int(self.x)
        self.int_y=int(self.y)
        
        self.xoffset=self.int_x+bgoffset_x2
        self.yoffset=self.int_y+bgoffset_y2
        print("planet")
        
        #screen.blit(self.planet_screen,(0,0))
        screen.blit(self.planet_screen,(self.xoffset,self.yoffset))
