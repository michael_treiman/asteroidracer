import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Pulsar(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen, spawn_rect):
        """Create a bullet object at the ship's current position."""
        super(Pulsar, self).__init__()
        self.screen = screen
        """spawnrect_temp=spawnrect.copy()
        spawnrect_temp.width*=ai_settings.closest_bg_layer_factor
        spawnrect_temp.height*=ai_settings.closest_bg_layer_factor"""
        # Create a bullet rect at (0,0) and then set the correct position.
       
        #Store the bullet's position as a decimal value
        self.color_index=1
        self.ball_color=self.getballcolorfromindex(self.color_index)
        self.x = 300
        self.y = 400
        
        self.offsetfactor=random.randrange(5,15)
        self.screen_rect = screen.get_rect()
        self.period=random.randrange(30,150)
        self.radius = int(random.randrange(6,15)/self.offsetfactor)
        self.x=random.randrange(spawn_rect.left,spawn_rect.right)
        self.y=random.randrange(spawn_rect.top,spawn_rect.bottom)
    
    def update(self):
        """Move the bullet up the screen."""
        #update the decimal position of the bullet
        #Update the rect position.
        
    def draw_pulsar(self,gameRect,bgoffset_x,bgoffset_y,state):
        """Draw the bullet to the screen."""
        #offset factor determines how far away this layer is
        temp_rad=self.radius
        
        top=self.y-temp_rad
        bottom=self.y+temp_rad
        left=self.x-temp_rad
        right=self.x+temp_rad
        if (gameRect.collidepoint(left,top) or gameRect.collidepoint(left,bottom) or gameRect.collidepoint(right,top) or gameRect.collidepoint(right,bottom)):

            phase=state.frames_elapsed%self.period
        
            if phase>6:
                return
        
            temp_rad=self.radius
        
            if phase==0:
                temp_rad*=1
            elif phase==1:
                temp_rad*=2
            elif phase==2:
                temp_rad*=3
            elif phase==3:
                temp_rad*=4
            elif phase==4:
                temp_rad*=3
            elif phase==5:
                temp_rad*=2
            elif phase==6:
                temp_rad*=1

        
        bgoffset_x2=int(bgoffset_x/self.offsetfactor)
        bgoffset_y2=int(bgoffset_y/self.offsetfactor)
        
        self.int_x=int(self.x)
        self.int_y=int(self.y)
        
        
        self.xoffset=self.int_x+bgoffset_x2
        self.yoffset=self.int_y+bgoffset_y2

        pygame.draw.circle(self.screen,self.ball_color,[self.xoffset, self.yoffset],temp_rad)
    
 
    def getballcolorfromindex(self,color_index):
        if color_index==1:
            return (255,255,255)
        elif color_index==2:
            return (80,255,220)
        elif color_index==3:
            return (200,80,255)
        return (80,80,255)


