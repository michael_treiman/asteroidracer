from tkinter import *
from tkinter import messagebox
import sys
import pygame
import time
from pygame.sprite import Group
import game_functions as gf
from button2 import Button2
from state import State
from settings import Settings
from ship import Ship
from asteroid import Asteroid
from asteroid_pixel import AsteroidPixel
from starfield import Starfield
from comet import Comet
from planet import Planet
from pulsar import Pulsar
from timer import Timer
from finish_line import Finish_line
from text_box import Text_box
from text2 import Text2
from game_config import Game_config
import TheBlackOfSpace
import urllib.request

#urllib.request.urlopen("http://example.com/foo/bar").read()


def clear_opponent(*args):
    opponentvar.set('-')



def set_params(*args):
    i=0
    opponent_str = opponentvar.get()
    taunt_index=IntVar()
    taunt_index.set(0)
    def next_opp_msg(*args):
        taunt_index.set(taunt_index.get()+1)
        if taunt_index.get()==len(taunt_str)-1:
            NextButton.config(text="Race")
        elif taunt_index.get()>=len(taunt_str):
            NextButton.config(text="Next")
            oppTauntStr.set("")
            set_story_mode()
        oppTauntStr.set(taunt_str[taunt_index.get()])
    if opponent_str=='Sterling McGovern - ★':
        state.opponent_id=1
        seed.set("f1fy")
        astshapevar.set('Rough')
        asteroiddensityvar.set('Low')
        taunt_str= [
        "I don't think I've seen you around here. Are you new?",
        "My name's Sterling McGovern. I come here to hang out with the boys and watch some asteroid races.",
        "Sometimes I like to try my hand at a race. These small craft aren't my specialty, though.",
        "In charge of one of the big tankers, I would smoke all of these people.",
        "What do you say, want to go?"
        ]
        print(i)
        taunt_str_len=len(taunt_str)
        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( root, textvariable=oppTauntStr )
        oppTauntMsg.grid(row = 6)


        NextButton = Button(mainframe, text="Next",command=next_opp_msg)
        NextButton.grid(row = 7, column = 3)

    elif opponent_str=='Zaphod Beeblebrox - ★★':
        state.opponent_id=2
        seed.set("f1fp")
        astshapevar.set('Rough')
        asteroiddensityvar.set('Medium')
        taunt_str= [
        "Howdy, friend. Do you like my sunglasses? Pretty fashionable, huh?",
        "I'm Zaphod Beeblebrox, former President of the galaxy. You may have heard of me.",
        "No? You must just be living under a rock.",
        "After retiring from public service I decided I'd give back to the community.",
        "Unfortunately, that was dreadfully dull. So instead I took up asteroid racing."
        ]

        taunt_str_len=len(taunt_str)
        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( root, textvariable=oppTauntStr )
        oppTauntMsg.grid(row = 6)


        NextButton = Button(mainframe, text="Next",command=next_opp_msg)
        NextButton.grid(row = 7, column = 3)
        
    elif opponent_str=='Grease Wingstream - ★★★':
        state.opponent_id=3
        seed.set("f1fbb")
        astshapevar.set('Rough')
        asteroiddensityvar.set('High')
        taunt_str= [
        "My ship is quite a beauty, isn't it? It always attracts the eye of passersby.",
        "I have put a lot of work into it. Is that your ship?",
        "Oh, you might want to spend some time cleaning it up.",
        "But hey, looks aren't everything, right? I'm Grease Wingstream, former pilot of the Galactic Navy.",
        "What, you really want to race? You think a rockhopper like you can keep up? I've got military training!",
        "Don't feel bad when you lose. Just don't do anything stupid."
        ]

        taunt_str_len=len(taunt_str)
        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( root, textvariable=oppTauntStr )
        oppTauntMsg.grid(row = 6)


        NextButton = Button(mainframe, text="Next",command=next_opp_msg)
        NextButton.grid(row = 7, column = 3)

    elif opponent_str=='Zebulon Hazard - ★★★★':
        state.opponent_id=4
        seed.set("f1fq")
        astshapevar.set('Rough')
        asteroiddensityvar.set('Medium')
        taunt_str= [
        "Racing is an artform. We express ourselves through motion.",
        "I am always looking for the perfect race - one that melds man, machine, and nature.",
        "Sadly, the people in this circuit seem to only have textbook skills. No creativity!",
        "Why don't you show me your flying artistry?"
        ]

        taunt_str_len=len(taunt_str)
        oppTauntStr = StringVar()

        oppTauntStr.set(taunt_str[taunt_index.get()])
        oppTauntMsg = Message( root, textvariable=oppTauntStr )
        oppTauntMsg.grid(row = 6)


        NextButton = Button(mainframe, text="Next",command=next_opp_msg)
        NextButton.grid(row = 7, column = 3)

    else:
        state.opponent_id=-1

def host_server():
    #Eventually this will set up the game settings from the opponent string
    port_number = port.get()
    ip_address = ip.get()
    print(port_number,ip_address)

    server = TTTServer()
    server.bind(port_number)


def connect_to_server():



    port_number = port.get()
    ip_address = ip.get()
    print(port_number,ip_address)
    client = TTTClient()
    client.connect(ip_address, port_number)

def set_opponent_params(opponent_str):
    #Eventually this will set up the game settings from the opponent string
    blorgus=0

def set_free_play_mode():
    print("The free play button was pushed")
    state.free_play=True
    state.time_trial=False
    state.story_mode=False

    enter_game()
def set_time_trial_mode():
    print("The time trial button was pushed")
    state.free_play=False
    state.time_trial=True
    state.story_mode=False

    enter_game()
def set_story_mode():
    print("The story mode button was pushed")
    state.free_play=False
    state.time_trial=False
    state.story_mode=True
    
    set_opponent_params("sad")
    enter_game()

def set_options_mode():
    print("The options button was pushed")
    options= not options


def set_credits_mode():
    print("The credits button was pushed")
    credits= not credits


def client_exit():
    print("The exit button was pushed")
    sys.exit()

def enter_game():
    play_music=musicVar.get()
    state.window_active=True
    if play_music==1:
        ai_settings.play_music=True
    else:
        ai_settings.play_music=False
    c_scheme = colorschemevar.get()
    

    if c_scheme=='Welcome to Space':
        ai_settings.color_scheme=1

    elif c_scheme=='Passion and Rage':
        ai_settings.color_scheme=2

    elif c_scheme=='Water':
        ai_settings.color_scheme=3

    elif c_scheme=='Jewels':
        ai_settings.color_scheme=4
        
    else:
        ai_settings.color_scheme=1

    ast_shape=astshapevar.get()

    if ast_shape=='Rough':
        ai_settings.asteroid_shape=1
    elif ast_shape=='Cigars':
        ai_settings.asteroid_shape=2
    elif ast_shape=='Jewels':
        ai_settings.asteroid_shape=3
    else:
        ai_settings.asteroid_shape=1

    opponent_str = opponentvar.get()

    if opponent_str=='Sterling McGovern - ★':
        state.opponent_id=1

    elif opponent_str=='Zaphod Beeblebrox - ★★':
        state.opponent_id=2

    elif opponent_str=='Grease Wingstream - ★★★':
        state.opponent_id=3

    elif opponent_str=='Zebulon Hazard - ★★★★':
        state.opponent_id=4

    else:
        state.opponent_id=-1

    ast_d_str = asteroiddensityvar.get()
    if ast_d_str=='Double Extreme':
        ai_settings.ast_grid_count = 400
    elif ast_d_str=='Extreme':
        ai_settings.ast_grid_count = 200
    elif ast_d_str=='High':
        ai_settings.ast_grid_count = 100
    elif ast_d_str=='Medium':
        ai_settings.ast_grid_count = 50
    elif ast_d_str=='Low':
        ai_settings.ast_grid_count = 20
    elif ast_d_str=='Smooth Sailing':
        ai_settings.ast_grid_count = 4
    else:
        ai_settings.ast_grid_count = 50

    state.seed_string=seed.get()
    print("seed: ",state.seed_string)

    TheBlackOfSpace.run_game(ai_settings, state)


root = Tk()
root.title("Asteroid Racer")

ai_settings = Settings()
state = State()
# Add a grid
mainframe = Frame(root)
mainframe.grid(column=0,row=0, sticky=(N,W,E,S) )
mainframe.columnconfigure(0, weight = 1)
mainframe.rowconfigure(0, weight = 1)

# Create a Tkinter variable

# Dictionary with options

options=False
credits=False


freePlayButton = Button(mainframe, text="Free Play",command=set_free_play_mode)
timeTrialButton = Button(mainframe, text="Time Trial",command=set_time_trial_mode)
#storyModeButton = Button(mainframe, text="Story Mode",command=set_story_mode)
optionsButton = Button(mainframe, text="Options",command=set_options_mode)
creditsButton = Button(mainframe, text="Credits",command=set_credits_mode)

quitButton = Button(mainframe, text="Exit",command=client_exit)
hostServerButton = Button(mainframe, text="Host server",command=host_server)
connectToServerButton = Button(mainframe, text="Connect to server",command=connect_to_server)


musicVar = IntVar()




colorschemevar = StringVar(root)
colorscheme_choices = ['Welcome to Space','Passion and Rage','Water','Jewels']
colorschemevar.set('Welcome to Space')
colorschemePopupMenu = OptionMenu(mainframe, colorschemevar, *colorscheme_choices)


asteroiddensityvar = StringVar(root)
asteroiddensity_choices = [ 'Double Extreme','Extreme','High','Medium','Low','Smooth Sailing']
asteroiddensityvar.set('Medium')
asteroiddensityPopupMenu = OptionMenu(mainframe, asteroiddensityvar, *asteroiddensity_choices, command=clear_opponent)
asteroiddensityPopupMenu.grid(row = 2, column =4)

opponentvar = StringVar(root)
opponent_choices = [ 'Choose an Opponent','-','Sterling McGovern - ★','Zaphod Beeblebrox - ★★','Grease Wingstream - ★★★','Zebulon Hazard - ★★★★']
opponentvar.set('Choose an Opponent')
opponentPopupMenu = OptionMenu(mainframe, opponentvar, *opponent_choices, command=set_params)
opponentPopupMenu.grid(row = 4, column =1)

astshapevar = StringVar(root)
astshape_choices = ['Rough','Cigars']
astshapevar.set('Rough')
astshapePopupMenu = OptionMenu(mainframe, astshapevar, *astshape_choices, command=clear_opponent)
astshapePopupMenu.grid(row = 2, column =2)

###IP and ports entry

ip=StringVar()
ipEntry = Entry(root, textvariable=ip)
ip.set("127.0.0.1")

ipEntryLabelStr = StringVar()
ipEntryLabelStr.set("Server IP address:")
ipEntryLabel = Message( root, textvariable=ipEntryLabelStr )

port=StringVar()
portEntry = Entry(root, textvariable=port)
port.set("6666")

portEntryLabelStr = StringVar()
portEntryLabelStr.set("Port number:")
portEntryLabel = Message( root, textvariable=portEntryLabelStr )

seed=StringVar()
seedEntry = Entry(root, textvariable=seed)
seed.set("f1fy")

seedEntryLabelStr = StringVar()
seedEntryLabelStr.set("World seed:")
seedEntryLabel = Message( root, textvariable= seedEntryLabelStr )



freePlayButton.grid(row = 2, column =1)
timeTrialButton.grid(row = 3, column =1)
#storyModeButton.grid(row = 4, column =1)
optionsButton.grid(row = 5, column =1)
creditsButton.grid(row = 6, column =1)
quitButton.grid(row = 7, column =1)
hostServerButton.grid(row = 1, column =5)

connectToServerButton.grid(row = 2, column =5)
colorschemePopupMenu.grid(row = 2, column =3)

Checkbutton(mainframe, text="Play Music", variable= musicVar).grid(row=1, sticky=W)
Label(mainframe, text="Choose a color scheme").grid(row = 1, column = 3)
Label(mainframe, text="Choose asteroid density").grid(row = 1, column = 4)
#Label(mainframe, text="Choose an opponent").grid(row = 1, column = 2)
Label(mainframe, text="Choose an asteroid shape").grid(row = 1, column = 2)

portEntry.grid(row = 8, column =7)
portEntryLabel.grid(row = 8, column =6)

seedEntry.grid(row = 7, column =4)
seedEntryLabel.grid(row = 7, column =3)

ipEntry.grid(row = 7, column =7)
ipEntryLabel.grid(row = 7, column =6)


# on change dropdown value
def change_dropdown(*args):
    print( opponentvar.get() )

# link function to change dropdown
opponentvar.trace('w', change_dropdown)

root.mainloop()


