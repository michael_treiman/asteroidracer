import sys

import pygame
import time
import random
import numpy
from pygame.sprite import Group
import game_functions as gf
from button2 import Button2
from state import State
from settings import Settings
from ship import Ship
from asteroid import Asteroid
from asteroid_pixel import AsteroidPixel
from starfield import Starfield
from comet import Comet
from planet import Planet
from pulsar import Pulsar
from timer import Timer
from finish_line import Finish_line
from finish_line2 import Finish_line2
from guide import Guide
from rails import Rails
from text_box import Text_box
import run_records


class Game_config():

    def __init__(self, ai_settings, state, screen, run_id=0):
   
        
        self.screen=screen


        #Run0 = Medium
        #Run1 = Easy
        #Run2 = Hard
        #Run3 = Expert
        if state.opponent_present:
            self.blue_event=run_records.get_run(state.opponent_id)
            
        self.ship = Ship(ai_settings,screen)
    
    
        self.ship_blue = Ship(ai_settings,screen,1)
        self.ship_blue.tracer_active=False

        self.bullets = Group()
        self.charges = Group()
        self.asteroids = Group()
        self.asteroid_pixels = Group()
        self.comets = Group()
        self.exhausts = Group()
        self.planets = Group()
        new_planet = Planet(ai_settings, screen)
        self.planets.add(new_planet)
        print("pl",self.planets)
        print("bob")
        self.starfield_pixels1 = Group()
        self.starfield_pixels2 = Group()
        self.starfield_pixels3 = Group()
    
        self.gameRect=screen.get_rect()
        self.bgrect_big=self.gameRect
   
        self.spawn_rect=pygame.Rect(0,0,self.gameRect.right,self.gameRect.bottom)

        random.seed(state.seed_string)

        """for i in range (1,ai_settings.num_asteroids):
            new_asteroid = Asteroid(ai_settings,screen,self.spawn_rect)
            self.asteroids.add(new_asteroid)"""
            
        for i in range (1,ai_settings.num_comets):
            new_comet = Comet(ai_settings, screen,self.spawn_rect)
            self.comets.add(new_comet)
    
        self.start_line = Finish_line(ai_settings, screen,ai_settings.start_line_x)
        self.finish_line = Finish_line(ai_settings, screen,ai_settings.finish_line_x)

        #last 4 coordinates are: start x, start y, end x, end y
        if run_id==0:
            self.lap_line0 = Finish_line2(ai_settings,state, screen,111, 389, 296, 701)
            self.lap_line1 = Finish_line2(ai_settings,state, screen,-590, 930, -205, 1223)
            self.lap_line2 = Finish_line2(ai_settings,state, screen,-823, 1785, -517, 1832)
            self.lap_line3 = Finish_line2(ai_settings,state, screen,-400, 2567, -429, 2915)
            self.lap_line4 = Finish_line2(ai_settings,state, screen,290, 2425, 311, 2856)
            self.lap_line5 = Finish_line2(ai_settings,state, screen,1937, 2503, 1724, 2125)
            self.lap_line6 = Finish_line2(ai_settings,state, screen,2687, 1413, 2364, 1274)
            self.lap_line7 = Finish_line2(ai_settings,state, screen,2725, 77, 2339, 143)
            self.lap_line8 = Finish_line2(ai_settings,state, screen,1632, -359, 1670, -52)
            self.lap_line9 = Finish_line2(ai_settings,state, screen,612, -586, 662, -308)
            self.lap_line10 = Finish_line2(ai_settings,state, screen,535, 207, 197, 193)
        
            guidepoly = numpy.array([
            [ 300 , 500,1 ],[ 288 , 506,1 ],[ 239 , 525,1 ],[ 158 , 566,1 ],
            [ 53 , 639,1 ],[ -73 , 745,1 ],[ -222 , 885,1 ],[ -366 , 1061,1 ],
            [ -496 , 1272,1 ],[ -599 , 1506,1 ],[ -662 , 1743,1 ],[ -687 , 1976,1 ],
            [ -676 , 2193,1 ],[ -652 , 2375,1 ],[ -622 , 2517,1 ],[ -587 , 2620,1 ],
            [ -532 , 2692,1 ],[ -459 , 2733,1 ],[ -363 , 2744,1 ],[ -231 , 2742,1 ],
            [ -66 , 2718,1 ],[ 134 , 2683,1 ],[ 374 , 2655,1 ],[ 653 , 2633,1 ],
            [ 966 , 2601,1 ],[ 1269 , 2537,1 ],[ 1543 , 2447,1 ],[ 1785 , 2331,1 ],
            [ 1998 , 2189,1 ],[ 2181 , 2020,1 ],[ 2333 , 1826,1 ],[ 2454 , 1605,1 ],
            [ 2546 , 1358,1 ],[ 2607 , 1085,1 ],[ 2633 , 810,1 ],[ 2631 , 560,1 ],
            [ 2611 , 345,1 ],[ 2572 , 166,1 ],[ 2515 , 23,1 ],[ 2439 , -84,1 ],
            [ 2344 , -157,1 ],[ 2224 , -201,1 ],[ 2068 , -227,1 ],[ 1877 , -234,1 ],
            [ 1661 , -213,1 ],[ 1434 , -198,1 ],[ 1238 , -204,1 ],[ 1067 , -240,1 ],
            [ 911 , -312,1 ],[ 782 , -406,1 ],[ 662 , -473,1 ],[ 558 , -502,1 ],
            [ 480 , -503,1 ],[ 426 , -472,1 ],[ 405 , -422,1 ],[ 409 , -350,1 ],
            [ 401 , -241,1 ],[ 389 , -94,1 ],[ 383 , 73,1 ],[ 385 , 240,1 ],
            [ 404 , 386,1 ],[ 417 , 497,1 ],[ 411 , 574,1 ],[ 389 , 615,1 ],
            [ 363 , 618,1 ],[ 328 , 585,1 ],[ 297 , 536,1 ],[ 290 , 504,1 ],
            [ 277 , 488,1]
            ])
            
            rail1poly = numpy.array([
            [285, 597,  5],[379, 675, 5],[564, 756,  5],
            [693, 772,  5],[827, 773, 5],[944, 746,  5],[1034, 701, 5],
            [1134, 619, 5],[1177, 508,5],[1189, 369, 5],[1175, 243, 5],
            [1110, 112, 5],[1035, 34, 5],[968, 4,    5],[845, 4,    5],
            [758, 3,    5],[648, 3,   5],[527, 7,    5],[403, 14,   5],
            [325, 55,   5],[240, 114, 5],[193, 203,  5],[180, 304,  5],
            [208, 408,  5],[237, 488, 5],[262, 544,  5],[285, 597, 5]])

            rail2poly = numpy.array([
            [359, 511, 5],[410, 526, 5],[473, 567, 5],[557, 591, 5],
            [652, 606, 5],[738, 601, 5],[819, 587, 5],[876, 561, 5],
            [918, 524, 5],[957, 471, 5],[996, 397, 5],[993, 357, 5],
            [977, 311, 5],[938, 255, 5],[902, 208, 5],[849, 170, 5],
            [794, 148, 5],[730, 135, 5],[652, 128, 5],[589, 133, 5],
            [531, 132, 5],[468, 138, 5],[405, 163, 5],[369, 194, 5],
            [351, 227, 5],[327, 290, 5],[320, 343, 5],[320, 428, 5],
            [345, 491, 5],[359, 511, 5]])
        
        elif run_id==1:
            blorgus=0



        self.guide = Guide(ai_settings,state, screen,guidepoly)
        self.rails = Rails(ai_settings,state, screen,rail1poly,rail2poly)
        
        self.grid_x=0
        self.grid_y=0
        self.grid_old_x=0
        self.grid_old_y=0
        self.grid_update=True
        self.pixels_active=set()
        self.pixels_active_old=set()

        self.s1_grid_old_x=0
        self.s1_grid_old_y=0
        self.s1_grid_update=True
        self.s1_pixels_active=set()
        self.s1_pixels_active_old=set()
        self.s1_layer=30

        self.s2_grid_old_x=0
        self.s2_grid_old_y=0
        self.s2_grid_update=True
        self.s2_pixels_active=set()
        self.s2_pixels_active_old=set()
        self.s2_layer=15

        self.s3_grid_old_x=0
        self.s3_grid_old_y=0
        self.s3_grid_update=True
        self.s3_pixels_active=set()
        self.s3_pixels_active_old=set()
        self.s3_layer=10
        
    def draw_lap_lines(self,state,ai_settings):
    
        """self.lap_line0.draw_finish_line(self.gameRect,state)
        self.lap_line1.draw_finish_line(self.gameRect,state)
        self.lap_line2.draw_finish_line(self.gameRect,state)
        self.lap_line3.draw_finish_line(self.gameRect,state)
        self.lap_line4.draw_finish_line(self.gameRect,state)
        self.lap_line5.draw_finish_line(self.gameRect,state)
        self.lap_line6.draw_finish_line(self.gameRect,state)
        self.lap_line7.draw_finish_line(self.gameRect,state)
        self.lap_line8.draw_finish_line(self.gameRect,state)
        self.lap_line9.draw_finish_line(self.gameRect,state)
        self.lap_line10.draw_finish_line(self.gameRect,state)"""
        
        #self.guide.draw_guide(self.gameRect,state,ai_settings)
        self.guide.draw_mini_map(self.gameRect,state,ai_settings)
        self.rails.draw_rails(self.gameRect,state,ai_settings)

    def scroll_grids(self,state,ai_settings,screen):

        ship_global_x=self.ship.centerx-state.bgoffset_x
        ship_global_y=self.ship.centery-state.bgoffset_y

        ############################

        self.grid_x=ship_global_x//1000
        self.grid_y=ship_global_y//1000

        #if the ship has moved grids, we change the active grids
        if not (self.grid_x == self.grid_old_x and self.grid_y == self.grid_old_y):
            self.grid_update=True
        self.grid_old_x=self.grid_x
        self.grid_old_y=self.grid_y

        if self.grid_update:
            self.pixels_active=set([
            (self.grid_x-1,self.grid_y-1),(self.grid_x,self.grid_y-1),(self.grid_x+1,self.grid_y-1),
            (self.grid_x-1,self.grid_y),(self.grid_x,self.grid_y),(self.grid_x+1,self.grid_y),
            (self.grid_x-1,self.grid_y+1),(self.grid_x,self.grid_y+1),(self.grid_x+1,self.grid_y+1)
            ])
            
            #set operations
            #s.difference(t)    s - t    new set with elements in s but not in t

            grids_to_add=self.pixels_active-self.pixels_active_old
            grids_to_remove=self.pixels_active_old-self.pixels_active
            
            for asteroid_pixel in self.asteroid_pixels:
                #set operations
                #x in s         test x for membership in s
                if (asteroid_pixel.i,asteroid_pixel.j) in grids_to_remove:
                    self.asteroid_pixels.remove(asteroid_pixel)

            for grid_to_add in grids_to_add:
                new_asteroid_pixel = AsteroidPixel(grid_to_add[0],grid_to_add[1],ai_settings.ast_grid_count,state.seed_string,ai_settings,screen)
                self.asteroid_pixels.add(new_asteroid_pixel)
            
            self.pixels_active_old=self.pixels_active
            #print(pixels_active)
            self.grid_update=False

        ############################
        ############################
        
        ###1Eventually make this into a class
    
        self.s1_grid_x=(-state.bgoffset_x/self.s1_layer-100)//ai_settings.starfield_px_wd
        self.s1_grid_y=(-state.bgoffset_y/self.s1_layer-100)//ai_settings.starfield_px_ht
        #print("ship_global_x,ship_global_y: ",ship_global_x,ship_global_y)
        
        if not (self.s1_grid_x == self.s1_grid_old_x and self.s1_grid_y == self.s1_grid_old_y):
            self.s1_grid_update=True
        self.s1_grid_old_x=self.s1_grid_x
        self.s1_grid_old_y=self.s1_grid_y

        if self.s1_grid_update:
        
        
        
            s1_pixels_active=set([
            (self.s1_grid_x,self.s1_grid_y),(self.s1_grid_x+1,self.s1_grid_y),(self.s1_grid_x+2,self.s1_grid_y),
            (self.s1_grid_x,self.s1_grid_y+1),(self.s1_grid_x+1,self.s1_grid_y+1),(self.s1_grid_x+2,self.s1_grid_y+1),
            (self.s1_grid_x,self.s1_grid_y+2),(self.s1_grid_x+1,self.s1_grid_y+2),(self.s1_grid_x+2,self.s1_grid_y+2)
            ])
            
            #set operations
            #s.difference(t)    s - t    new set with elements in s but not in t

            s1_grids_to_add=self.s1_pixels_active-self.s1_pixels_active_old
            s1_grids_to_remove=self.s1_pixels_active_old-self.s1_pixels_active
            """print("s1_grids_to_add: ",s1_grids_to_add)
            print("s1_grids_to_remove: ", s1_grids_to_remove)
            print("s1_pixels_active: ",s1_pixels_active)
            print("s1_pixels_active_old: ",s1_pixels_active_old)"""

            self.s1_pixels_active_old=self.s1_pixels_active
            
            self.s1_grid_update=False

            for starfield in self.starfield_pixels1:
                #set operations
                #x in s         test x for membership in s
                if (starfield.i,starfield.j) in s1_grids_to_remove:
                    blorgus=0
                    self.starfield_pixels1.remove(starfield)

            for s1_grid_to_add in s1_grids_to_add:
                #Starfield wants params: i,j,count,typevar,layer,ai_settings
                new_starfield = Starfield(s1_grid_to_add[0],s1_grid_to_add[1],ai_settings.star_count,state.seed_string,s1_layer,ai_settings)
                new_starfield.render_starfield()
                self.starfield_pixels1.add(new_starfield)
        
        ###1end of starfield grid manager
        
        ###2Eventually make this into a class

        self.s2_grid_x=(-state.bgoffset_x/self.s2_layer-100)//ai_settings.starfield_px_wd
        self.s2_grid_y=(-state.bgoffset_y/self.s2_layer-100)//ai_settings.starfield_px_ht
        #print("ship_global_x,ship_global_y: ",ship_global_x,ship_global_y)

        if not (self.s2_grid_x == self.s2_grid_old_x and self.s2_grid_y == self.s2_grid_old_y):
            self.s2_grid_update=True
        self.s2_grid_old_x=self.s2_grid_x
        self.s2_grid_old_y=self.s2_grid_y

        if self.s2_grid_update:
        
        
        
            self.s2_pixels_active=set([
            (self.s2_grid_x,self.s2_grid_y),(self.s2_grid_x+1,self.s2_grid_y),(self.s2_grid_x+2,self.s2_grid_y),
            (self.s2_grid_x,self.s2_grid_y+1),(self.s2_grid_x+1,self.s2_grid_y+1),(self.s2_grid_x+2,self.s2_grid_y+1),
            (self.s2_grid_x,self.s2_grid_y+2),(self.s2_grid_x+1,self.s2_grid_y+2),(self.s2_grid_x+2,self.s2_grid_y+2)
            ])
            
            #set operations
            #s.difference(t)    s - t    new set with elements in s but not in t

            s2_grids_to_add=self.s2_pixels_active-self.s2_pixels_active_old
            s2_grids_to_remove=self.s2_pixels_active_old-self.s2_pixels_active
            """print("s2_grids_to_add: ",s2_grids_to_add)
            print("s2_grids_to_remove: ", s2_grids_to_remove)
            print("s2_pixels_active: ",s2_pixels_active)
            print("s2_pixels_active_old: ",s2_pixels_active_old)"""

            self.s2_pixels_active_old=self.s2_pixels_active
            
            self.s2_grid_update=False

            for starfield in self.starfield_pixels2:
                #set operations
                #x in s         test x for membership in s
                if (starfield.i,starfield.j) in s2_grids_to_remove:
                    blorgus=0
                    self.starfield_pixels2.remove(starfield)

            for s2_grid_to_add in s2_grids_to_add:
                #Starfield wants params: i,j,count,typevar,layer,ai_settings
                new_starfield = Starfield(s2_grid_to_add[0],s2_grid_to_add[1],ai_settings.star_count,state.seed_string,self.s2_layer,ai_settings)
                new_starfield.render_starfield()
                self.starfield_pixels2.add(new_starfield)

        ###2end of starfield grid manager

        ###3Eventually make this into a class

        self.s3_grid_x=(-state.bgoffset_x/self.s3_layer-100)//ai_settings.starfield_px_wd
        self.s3_grid_y=(-state.bgoffset_y/self.s3_layer-100)//ai_settings.starfield_px_ht
        #print("ship_global_x,ship_global_y: ",ship_global_x,ship_global_y)

        if not (self.s3_grid_x == self.s3_grid_old_x and self.s3_grid_y == self.s3_grid_old_y):
            self.s3_grid_update=True
        self.s3_grid_old_x=self.s3_grid_x
        self.s3_grid_old_y=self.s3_grid_y

        if self.s3_grid_update:
        
        
        
            self.s3_pixels_active=set([
            (self.s3_grid_x,self.s3_grid_y),(self.s3_grid_x+1,self.s3_grid_y),(self.s3_grid_x+2,self.s3_grid_y),
            (self.s3_grid_x,self.s3_grid_y+1),(self.s3_grid_x+1,self.s3_grid_y+1),(self.s3_grid_x+2,self.s3_grid_y+1),
            (self.s3_grid_x,self.s3_grid_y+2),(self.s3_grid_x+1,self.s3_grid_y+2),(self.s3_grid_x+2,self.s3_grid_y+2)
            ])
            
            #set operations
            #s.difference(t)    s - t    new set with elements in s but not in t

            s3_grids_to_add=self.s3_pixels_active-self.s3_pixels_active_old
            s3_grids_to_remove=self.s3_pixels_active_old-self.s3_pixels_active
            """print("s3_grids_to_add: ",s3_grids_to_add)
            print("s3_grids_to_remove: ", s3_grids_to_remove)
            print("s3_pixels_active: ",s3_pixels_active)
            print("s3_pixels_active_old: ",s3_pixels_active_old)"""

            self.s3_pixels_active_old=self.s3_pixels_active
            
            self.s3_grid_update=False

            for starfield in self.starfield_pixels3:
                #set operations
                #x in s         test x for membership in s
                if (starfield.i,starfield.j) in s3_grids_to_remove:
                    blorgus=0
                    self.starfield_pixels3.remove(starfield)

            for s3_grid_to_add in s3_grids_to_add:
                #Starfield wants params: i,j,count,typevar,layer,ai_settings
                new_starfield = Starfield(s3_grid_to_add[0],s3_grid_to_add[1],ai_settings.star_count,state.seed_string,self.s3_layer,ai_settings)
                new_starfield.render_starfield()
                self.starfield_pixels3.add(new_starfield)

        ###3end of starfield grid manager

