import sys

import pygame
import time
import random
import numpy
from pygame.sprite import Group
import game_functions as gf
from button2 import Button2
from state import State
from settings import Settings
from timer import Timer
from text_box import Text_box
from text2 import Text2
import run_records


class Text_config():

    def __init__(self, ai_settings, state, screen):

        self.menu_button = Button2(ai_settings, screen, "Menu")
        self.retry_button = Button2(ai_settings, screen, "Retry Run",75)

        self.timer = Timer(ai_settings, screen)
        self.record_timer = Timer(ai_settings, screen)
        
        self.lap1_info_text = Text2(ai_settings, screen, ''.join(state.lap1_list),100,2)
        self.lap2_info_text = Text2(ai_settings, screen, ''.join(state.lap2_list),150,2)
        self.lap3_info_text = Text2(ai_settings, screen, ''.join(state.lap3_list),200,2)
        self.lap4_info_text = Text2(ai_settings, screen, ''.join(state.lap4_list),250,2)

        self.get_ready_text = Text2(ai_settings, screen, "Get Ready",150,1)
        
        self.three_text = Text2(ai_settings, screen, "3",100,1)
        self.two_text = Text2(ai_settings, screen, "2",100,1)
        self.one_text = Text2(ai_settings, screen, "1",100,1)
        self.go_text = Text2(ai_settings, screen, "GO!",100,1)

        self.finish_text = Text2(ai_settings, screen, "Finish",100,1)
        self.you_win_text = Text2(ai_settings, screen, "YOU WIN",100,1)
        self.you_lose_text = Text2(ai_settings, screen, "YOU LOSE",100,1)


    def draw_text(self,state):
        self.menu_button.draw_button()
        self.retry_button.draw_button()

        self.timer.draw_timer(state)
        self.record_timer.draw_timer(state)
        self.lap1_info_text.draw_text()
        self.lap2_info_text.draw_text()
        self.lap3_info_text.draw_text()
        self.lap4_info_text.draw_text()
        fe=state.frames_elapsed
        fps=20
        sec1=1*fps
        sec2=2*fps
        sec3=3*fps
        sec4=4*fps
        sec5=5*fps
        sec6=6*fps
        sec7=7*fps
        sec8=8*fps
        
        get_ready=False
        three=False
        two=False
        one=False
        go=False
        finish=False
        you_win=False
        you_lose=False
        
        if fe<sec1:
            get_ready=True
        elif (fe>=sec1 and fe<sec2):
            three=True
        elif (fe>=sec2 and fe<sec3):
            two=True
        elif (fe>=sec3 and fe<sec4):
            one=True
        elif (fe>=sec4 and fe<sec8):
            go=True
        elif (state.crossed_finish and fe<state.crossed_finish_frame+sec3):
            finish=True
        elif (state.crossed_finish and fe>=state.crossed_finish_frame+sec3):
            if state.lose:
                self.you_lose_text.draw_text()
            else:
                self.you_win_text.draw_text()

        if get_ready:
            self.get_ready_text.draw_text()
        elif three:
            self.three_text.draw_text()
        elif two:
            self.two_text.draw_text()
        elif one:
            self.one_text.draw_text()
        elif go:
            self.go_text.draw_text()
            state.thrust_frozen=False
        elif finish:
            self.finish_text.draw_text()
        elif you_win:
            self.you_win_text.draw_text()
        elif you_lose:
            self.you_lose_text.draw_text()


