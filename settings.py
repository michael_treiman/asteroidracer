import random
class Settings():
    """A class to store all the settings for The Black of Space."""

    def __init__(self):
        """Initialize the game's settings."""
        #Screen settings
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (0,0,0)
        self.bg_menu_color = (0,0,0)
        self.ship_speed_factor = 1.5
        self.bullet_speed_factor = 1
        self.bullet_width = 5
        self.bullet_height = 5
        self.bullet_color = 255,255,255
        self.bullets_allowed=100
        self.comet_tail_dir= random.uniform(0,3.1415926*2)
        #closest_bg_layer_factor controls how fast the closest
        #background layer scrolls, which is also by what factor
        #larger than the background is the play space
        #self.closest_bg_layer_factor=10
        self.closest_bg_layer_factor=10
        self.mid_bg_layer_factor=15
        self.far_bg_layer_factor=20
        self.size_x = 2
        self.size_y = 2
        #self.size_x = 6
        #self.size_y = 6
        self.start_line_x = 500
        self.finish_line_x = 4000
        self.num_asteroids = 10
        self.ast_grid_count = 10
        #asteroid shape 1: rough. 2: cigars. 3: jewels
        self.asteroid_shape=3

        self.num_comets = 600
        self.num_pulsars = 15
        self.num_stars = 400
        self.star_count = 400
        #self.num_stars = 400*3*3
        self.color_scheme=0

        #standard tracer
        self.s_tracer_active=True
        #stop range tracer
        self.sr_tracer_active=True
        #parabolic tracer (where the ship will go if you boost)s
        self.p_tracer_active=True
        self.ship_start_coords=(300,500)
        self.ship_blue_start_coords=(300,500)
        self.ship_start_cleared_rad=50
        self.play_music=False
        self.ship_respawn_frames=60
        self.ship_col_frames = 2
        self.starfield_px_wd=800
        self.starfield_px_ht=500
        self.scroll_left = 300
        self.scroll_right = 800
        self.scroll_top = 300
        self.scroll_bottom =  300
        self.init_bgoffset_y=0
        
        #Right/left/top/bottom scroll maximum increments
        #This is used to make the scrolling action smooth, especially
        #when the player switches between scroll modes
        self.rsmi = 1
        self.lsmi = 1
        self.tsmi = 1
        self.bsmi = 1

        
