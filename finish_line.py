import pygame
import math
from pygame.sprite import Sprite
import game_functions
import numpy
import random


class Finish_line(Sprite):
    """A class to manage bullets fired from the ship."""

    def __init__(self, ai_settings, screen,x_pos):
        """Create a bullet object at the ship's current position."""
        super(Finish_line, self).__init__()
        self.screen = screen
                # Create a bullet rect at (0,0) and then set the correct position.
       
        #Store the bullet's position as a decimal value
        self.color_index=1
        self.color = (255,0,0)
        self.period = 15
        self.screen_rect = screen.get_rect()
        self.rect=pygame.Rect(x_pos,0,5,4000)
    
    def update(self):
        """Move the bullet up the screen."""
        
    def draw_finish_line(self,gameRect,state):
        """Draw the bullet to the screen."""
        #offset factor determines how far away this layer is
        

        #if (gameRect.collidepoint(left,top) or gameRect.collidepoint(right,bottom)):
        phase=state.frames_elapsed%self.period
        
        if phase>6:
            self.color_index=0
            
        elif phase==0:
            self.color_index=1
        elif phase==1:
            self.color_index=2
        elif phase==2:
            self.color_index=3
        elif phase==3:
            self.color_index=4
        elif phase==4:
            self.color_index=3
        elif phase==5:
            self.color_index=2
        elif phase==6:
            self.color_index=1
        self.color=self.getcolorfromindex(self.color_index)
        bgoffset_x2=int(state.bgoffset_x)
        bgoffset_y2=int(state.bgoffset_y)

        rect_move=self.rect.move(bgoffset_x2,0)
        pygame.draw.rect(self.screen, self.color,rect_move)
    
 
    def getcolorfromindex(self,color_index):
        if color_index==0:
            return (255,0,0)
        elif color_index==1:
            return (255,30,0)
        elif color_index==2:
            return (255,60,0)
        elif color_index==3:
            return (255,90,0)
        elif color_index==4:
            return (255,120,0)
        return (255,0,0)


